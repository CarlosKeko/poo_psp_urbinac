package m09uf2.impl.access;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.function.Consumer;
import java.util.logging.Logger;

import lib.fils.SafeQueue;
import m09uf2.prob.access.AccessReport;
import m09uf2.prob.access.SiteAccessService;

public class SiteAccessServiceImpl implements SiteAccessService, Runnable{
	
	static final Logger LOGGER = Logger.getLogger(SiteAccessServiceImpl.class.getName());
	private SafeQueue<PeticioServidor> cua;
	private Thread thread;
	
	public SiteAccessServiceImpl(int size) {
		cua = new SafeQueue<SiteAccessServiceImpl.PeticioServidor>(size);
		thread = new Thread(this, "Fill");
	}

	@Override
	public void request(String t, Consumer<AccessReport> consumer) throws InterruptedException {
		PeticioServidor peticio = new PeticioServidor(t, consumer);
		cua.put(peticio);
		
		if (!thread.isAlive()) {
			thread.start();
		}
		
	}

	@Override
	public void shutdown() {
		thread.interrupt();
	}
	
	static class PeticioServidor {
		
		private String url;
		private Consumer<AccessReport> consumer;
		
		public PeticioServidor(String url, Consumer<AccessReport> consumer) {
			this.url = url;
			this.consumer = consumer;
		}
		
	}

	@Override
	public void run() {
		
		synchronized (cua) {
			long took = -1;
			String server;
			while (true) {
				try {
					PeticioServidor peticio = cua.take();
					URL url = new URL(peticio.url);
					long millis = System.currentTimeMillis();
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				    conn.setConnectTimeout(6000);
				    conn.connect();				    
				    took = System.currentTimeMillis() - millis;
				    server = conn.getHeaderField("Server");
				    peticio.consumer.accept(new AccessReport(url.toString(), took, server));
					
				} catch (InterruptedException e) {
					LOGGER.info("Interrumpido!");
					break;
					
				} catch (IOException e) {
					LOGGER.info(e.getLocalizedMessage());
				}
			}
		}
	}

}
