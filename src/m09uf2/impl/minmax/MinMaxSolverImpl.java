package m09uf2.impl.minmax;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.logging.Logger;

import m09uf2.prob.minmax.MinMaxSolver;

public class MinMaxSolverImpl implements MinMaxSolver{

	private MinMaxCalc calc;
	static final Logger LOGGER = Logger.getLogger(MinMaxRun.class.getName());
	
//	public MinMaxSolverImpl() {
//		calc = new MinMaxCalc();
//	}
	
	public MinMaxSolverImpl(MinMaxCalc calc) {
		this.calc = calc;
	}
	
	/**
	 * Calcula el minim i el maxim de la mostra, 
	 * retornant un array amb dos elements: el minim i el maxim.
	 *  
	 * @param nums
	 * @return
	 */
	public double[] minMax(double[] nums) {
		
		List<Numero> numeros = new ArrayList<Numero>();
		List<Thread> threads = new ArrayList<Thread>();
		Queue<Double> queue = new LinkedList<Double>();
		
		for (int i = 0; i < nums.length; i++) {
			queue.add(nums[i]);
		}
		
		for (int i = 0; i < Runtime.getRuntime().availableProcessors(); i++) {
			numeros.add(new Numero(i, this.calc));
			threads.add(numeros.get(i).getThread());
			
		}
		
		double[] numsMax = new double[numeros.size()];
		double[] numsMin = new double[numeros.size()];
		
		for (int i = 0; i < numeros.size(); i++) {
			if (i == (numeros.size() - 1)) {
				for (int j = 0; j < (((int) Math.abs(nums.length)) / numeros.size()) + (nums.length % numeros.size()); j++) {
					numeros.get(i).getNumsAux().add(queue.poll());
				}
			}else {
				for (int j = 0; j < (((int) Math.abs(nums.length)) / numeros.size()); j++) {
					numeros.get(i).getNumsAux().add(queue.poll());
				}
			}
			threads.get(i).start();
			
		}
		
		for (int i = 0; i < threads.size(); i++) {
			try {
				threads.get(i).join();
				numsMax[i] = numeros.get(i).getMax();
				numsMin[i] = numeros.get(i).getMin();
			} catch (InterruptedException e) {
				LOGGER.info("Interrumpido...");
			}
		}
		
		return new double[] {calc.min(numsMin, 0, numsMin.length), calc.max(numsMax, 0, numsMax.length)};
	}

}
