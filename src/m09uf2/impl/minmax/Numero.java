package m09uf2.impl.minmax;

import java.util.ArrayList;
import java.util.List;

public class Numero implements Runnable{
	
	private MinMaxCalc calc;
	private Thread thread;
	private List<Double> numsAux;
	private double min;
	private double max;
	
	public Numero(int i, MinMaxCalc calc) {
		this.thread = new Thread(this, "Fill " + i);
		this.numsAux = new ArrayList<Double>();
		this.calc = calc;
	}
	
	public Thread getThread() {
		return this.thread;
	}

	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = min;
	}

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}
	
	public List<Double> getNumsAux() {
		return numsAux;
	}

	public void setNumsAux(List<Double> numsAux) {
		this.numsAux = numsAux;
	}
	
	@Override
	public void run() {
		
		double[] nums = new double[numsAux.size()];
	
		for (int i = 0; i < nums.length; i++) {
			nums[i] = numsAux.get(i);
		}
		
		this.max = calc.max(nums, 0, nums.length);
		this.min = calc.min(nums, 0, nums.length);
	}

}
