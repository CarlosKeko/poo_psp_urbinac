package m09uf2.impl.waittaskb;

import java.util.logging.Logger;

import m09uf2.prob.waittaskb.DoTask;
import m09uf2.prob.waittaskb.WaitTaskJoin;

public class WaitTaskJoinImpl<T> implements WaitTaskJoin<T>, Runnable {

	static final Logger LOGGER = Logger.getLogger(WaitTaskJoinImpl.class.getName());
	private Thread thread;
	private long millis;
	private DoTask<T> task;
	private T result;
	
	public WaitTaskJoinImpl() {
		thread = new Thread(this, "fill");
	}
	
	public void submit(long millis, DoTask<T> task) {	
		this.millis = millis;
		this.task = task;
		thread.start();
		
	}

	public T result() {
		try {
			thread.join();
		} catch (InterruptedException e) {
			LOGGER.info("Interrupted!");
			
		}
		
		return this.result;
	}

	public void cancel(T t) {
		this.result = t;
		thread.interrupt();
		
	}

	public void run() {
		
		try {
			Thread.sleep(millis);
			result = task.execute();
			
		} catch (InterruptedException e) {
			LOGGER.info("Interrupted!");
			
		}
		
	}

}
