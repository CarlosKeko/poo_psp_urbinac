package m09uf2.impl.waittaskb;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import m09uf2.prob.waittaskb.DoTask;
import m09uf2.prob.waittaskb.WaitTaskFuture;

public class WaitTaskFutureImpl<T> implements WaitTaskFuture<T>, Runnable {

	static final Logger LOGGER = Logger.getLogger(WaitTaskFutureImpl.class.getName());
	private CompletableFuture<T> cf;
	private DoTask<T> task;
	private Thread thread;
	private long millis;
	
	public WaitTaskFutureImpl() {
		cf =  new CompletableFuture<T>();
		thread = new Thread(this, "Fill");
	}
	
	public void submit(long millis, DoTask<T> task) {
		this.millis = millis;
		this.task = task;
		thread.start();
		
	}

	public Future<T> future() {
		return this.cf;
	}
	
	public void cancel(T t) {
		this.cf.complete(t);
		thread.interrupt();
		
	}

	public void run() {
		
		try {
			Thread.sleep(millis);
			this.cf.complete(task.execute());
			
		} catch (InterruptedException e) {
			LOGGER.info("Interrupted!");
		}
		
	}

}
