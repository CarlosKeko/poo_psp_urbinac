package m09uf2.impl.votacio;

import m09uf2.prob.votacio.LocalElectoral;
import m09uf2.prob.votacio.VotCalc;

public class LocalElectoralImpl implements LocalElectoral{

	private int localDins;
	private int numMeses;
	private int numCandidats;
	private VotCalc votcalc;
	private int[][] vots;
	
	public LocalElectoralImpl(VotCalc votcalc, int numMeses, int numCandidats) {
		this.votcalc = votcalc;
		this.numMeses = numMeses;
		this.numCandidats = numCandidats;
		this.localDins = 0;
		this.vots = new int[numMeses][numCandidats];
		
	}
	
	public int findMesa(long idVotant) {	
		return (int) (idVotant % this.numMeses);
		
	}

	public boolean votar(long idVotant, int mesa, int candidat) {	
		synchronized (this.vots[mesa]) {
			if (candidat >= 0 && candidat < this.numCandidats && mesa == findMesa(idVotant)) {
				this.vots[mesa][candidat] = this.votcalc.increment(this.vots[mesa][candidat]);
				return true;
				
			}else {
				return false;
				
			}
		}

	}

	public void entrada() {
		synchronized (this.vots) {
			this.localDins++;
		}
	
	}

	public void sortida() {
		synchronized (this.vots) {
			this.localDins--;
		}
	}

	public int presents() {
		return this.localDins;
		
	}

	@Override
	public int[] resultats() {
		int[] resultats = new int[this.numCandidats];
		
		for (int i = 0; i < this.vots.length; i++) {
			for (int j = 0; j < this.vots[0].length; j++) {
				resultats[j] = resultats[j] + this.vots[i][j];
			}
		}
		
		return resultats;
	}

}
