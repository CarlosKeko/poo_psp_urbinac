package m09uf2.impl.timer2;

import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.logging.Logger;

import m09uf2.impl.timer1.Timer1Impl;
import m09uf2.prob.timer2.Timer2;

public class Timer2Impl implements Timer2, Runnable {

	static final Logger LOGGER = Logger.getLogger(Timer1Impl.class.getName());

	volatile boolean acabar;
	private Thread thread;
	private ConcurrentHashMap<Integer, Alarma> alarmas;
	private int identificador = 0;

	public Timer2Impl() {
		alarmas = new ConcurrentHashMap<Integer, Alarma>();
		thread = new Thread(this, "timer");
	}

	@Override
	public int set(long millis, Consumer<Integer> handler) {
		identificador++;

		this.alarmas.put(identificador, new Alarma(millis, handler));

		if (!thread.isAlive()) {
			thread.start();

		}

		return identificador;
	}

	public void shutdown() {
		acabar = true;

	}

	@Override
	public void run() {
		while (!acabar) {
			synchronized (alarmas) {
				for (Integer key : this.alarmas.keySet()) {
					if (System.currentTimeMillis() == alarmas.get(key).getCurrentMillis()) {
						alarmas.get(key).getHandler().accept(key);
						alarmas.remove(key);
					}
				}
			}
		}
	}

}
