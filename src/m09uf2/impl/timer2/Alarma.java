package m09uf2.impl.timer2;

import java.util.function.Consumer;

public class Alarma {

	private long millis;
	private Consumer<Integer> handler;
	private boolean fet;
	private long currentMillis;
	
	public Alarma(long m, Consumer<Integer> h) {
		millis = m;
		currentMillis = System.currentTimeMillis() + millis;
		handler = h;
		fet = false;
	}
	
	public boolean getFet() {
		return this.fet;
	}
	
	public long getMillis() {
		return this.millis;
	}
	
	public Consumer<Integer> getHandler() {
		this.fet = true;
		return this.handler;
	}

	public long getCurrentMillis() {
		return currentMillis;
	}

	public void setCurrentMillis(long currentMillis) {
		this.currentMillis = currentMillis;
	}
	
}
