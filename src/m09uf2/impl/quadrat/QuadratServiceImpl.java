package m09uf2.impl.quadrat;

import java.util.concurrent.Future;
import java.util.logging.Logger;

import lib.fils.SafeQueue;

import java.util.concurrent.CompletableFuture;

import m09uf2.prob.quadrat.QuadratService;

public class QuadratServiceImpl implements QuadratService, Runnable{

	private SafeQueue<CompletableFuture<Double>> cuaFutures;
	private SafeQueue<Double> cua;
	private Thread thread;
	static final Logger LOGGER = Logger.getLogger(QuadratServiceImpl.class.getName());
	
	public QuadratServiceImpl(int midaCua) {
		cuaFutures = new SafeQueue<CompletableFuture<Double>>(midaCua);
		cua = new SafeQueue<Double>(midaCua);
		thread = new Thread(this, "Fill");
		thread.start();
	}
	
	public Future<Double> request(double num) throws InterruptedException {
		cua.put(num);
		return cuaFutures.take();
	}

	public void shutdown() {
		this.thread.interrupt();
		
	}

	public void run() {
		
		CompletableFuture<Double> cf;
		
		while (true) {
			try {		
				double num = this.cua.take();
				cf = new CompletableFuture<Double>();
				cf.complete(num * num);
				this.cuaFutures.put(cf);
				
			}catch (InterruptedException e) {
				LOGGER.info("Interrupted!");
				break;
			}
		}
		
	}

	
	

}
