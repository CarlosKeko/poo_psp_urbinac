package m09uf2.impl.minmaxf;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import m09uf2.prob.minmaxf.MinMaxCalc;
import m09uf2.prob.minmaxf.MinMaxSolver;

public class MinMaxSolverImpl implements MinMaxSolver{

	private MinMaxCalc calc;
	static final Logger LOGGER = Logger.getLogger(MinMaxSolverImpl.class.getName());
	
	public MinMaxSolverImpl(MinMaxCalc calc) {
		this.calc = calc;
		
	}
	
	@Override
	public double[] minMax(double[] nums) {
		int count = Runtime.getRuntime().availableProcessors();
		ExecutorService service = Executors.newCachedThreadPool();
		@SuppressWarnings("unchecked")
		Future<MyCallable>[] future = new Future[count];
		LOGGER.info("start " + count + " threads!");
		
		int from = 0;
		int part = nums.length / count;
		
		for (int i=0; i<count; i++) {			
			int to = i+1 == count? 
					nums.length : from + part;
			LOGGER.info("from " + from + " to " + to);
			
			MyCallable calla = new MyCallable(calc, nums, from, to);
			future[i] = service.submit(calla);
			from = to;
		}

		double[] mins = new double[count];
		double[] maxs = new double[count];
		
		for (int i=0; i<count; i++) {
			
			try {
				mins[i] = future[i].get().getMin();
				maxs[i] = future[i].get().getMax();
				
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 

		}
		
		service.shutdown();
		
		double min = calc.min(mins, 0, count);
		double max = calc.max(maxs, 0, count);
		return new double[] {min, max};
	}
	
	static class MyCallable implements Callable<MyCallable> {

		private MinMaxCalc calc;
		private double[] nums;
		private int from, to;
		private double min, max;
		
		public MyCallable(MinMaxCalc calc, double[] nums, int from, int to) {
			this.calc = calc;
			this.nums = nums;
			this.from = from;
			this.to = to;
		}
		
		public double getMin() { return min; }
		public double getMax() { return max; }
		
		@Override
		public MyCallable call() throws Exception {
			LOGGER.info("start!");
			min = calc.min(nums, from, to);
			max = calc.max(nums, from, to);		
			LOGGER.info("parcial min=" + min + ", max=" + max);
			return this;
		}
		
	}

}
