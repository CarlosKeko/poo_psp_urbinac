package m09uf2.impl.timer1;

import java.util.logging.Level;
import java.util.logging.Logger;

import m09uf2.prob.timer1.Timer1;

public class Timer1Impl implements Timer1{

	public class Temporizator implements Runnable {
		
		private long millis;
		
		public Temporizator(long millis) {
			this.millis = millis;
		}

		public void run() {
			//LOGGER.info("hola, soc el fil " + Thread.currentThread().getName());
			
			try {
				Thread.sleep(millis);
			} catch (InterruptedException ex) {
	        	LOGGER.log(Level.SEVERE, "interrupció", ex);
			}
			
		}
		
	}
	
	private long timer1Impl;
	private Thread thread;
	private static final Logger LOGGER = Logger.getLogger(Timer1Impl.class.getName());
	
	public Timer1Impl(long millis) {
		this.timer1Impl = millis;
	}
	
	public long millis() {
		return this.timer1Impl;
	}

	public void go() {
		Runnable timer = new Temporizator(millis());
		this.thread = new Thread(timer, "fill");
		this.thread.start();
	}

	public void cancel() {
		this.thread.interrupt();
		
	}

	public void hold() {
		try {
			thread.join();
		} catch (InterruptedException ex) {
        	LOGGER.log(Level.SEVERE, "interrupció", ex);
		}
		
	}

	public boolean done() {
		return !thread.isAlive();
	}
	
	public Thread getThread() {
		return this.thread;
	}
	
}