package m03uf5.impl.exam2b;

import javafx.application.Application;
import javafx.stage.Stage;
import m03uf5.impl.exam2b.CalcHistContract.CalcHistPresenter;
import m03uf5.impl.exam2b.CalcHistContract.CalcHistView;
import m03uf5.impl.exam2b.presenter.CalcHistPresenterImpl;
import m03uf5.impl.exam2b.view.CalcHistViewImpl;

public class CalcHistApp extends Application {

	private CalcHistView v;
	private CalcHistPresenter p;
	
	public void start(Stage primaryStage) throws Exception {
		p = new CalcHistPresenterImpl();
		v = new CalcHistViewImpl(primaryStage);

		p.setView(v);
		v.setPresenter(p);
		
	}
	
	public static void main(String[] args) {
		launch(args);
	}

}
