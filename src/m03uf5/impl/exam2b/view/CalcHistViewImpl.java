package m03uf5.impl.exam2b.view;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import m03uf5.impl.exam2b.CalcHistContract;
import m03uf5.impl.exam2b.CalcHistContract.CalcHistPresenter;

public class CalcHistViewImpl implements CalcHistContract.CalcHistView {

	private CalcHistPresenter presenter;
	private Label lbl;
	private TextField textField;
	private Button btnEnrrera;
	private Button btnEndavant;
	
	public CalcHistViewImpl(Stage primaryStage) {
		setStage(primaryStage);
	}

	public void setPresenter(CalcHistPresenter p) {
		this.presenter = p;
		
	}

	public void mostra(String resultat) {
		this.lbl.setText(resultat);
		
	}
	
	public void modificarTextField(String resultat) {
		this.textField.setText(resultat);
	}
	
	public void habilitarEnrrera() {
		this.btnEnrrera.setDisable(false);
	}
	
	public void deshabilitarEnrrera() {
		this.btnEnrrera.setDisable(true);
	}
	
	public void habilitarEndavant() {
		this.btnEndavant.setDisable(false);
	}
	
	public void deshabilitarEndavant() {
		this.btnEndavant.setDisable(true);
	}
	
	private void setStage(Stage primaryStage) {
		primaryStage.setTitle("Calculadora");
		
        textField = new TextField();
        textField.setText("");
        textField.setPrefHeight(80);
        textField.setPrefWidth(80);
        
        Button btn = new Button();
        btn.setText("Calcula");
        btn.setPrefHeight(80);
        btn.setPrefWidth(80);
        
        btnEnrrera = new Button();
        btnEnrrera.setText("<--");
        btnEnrrera.setDisable(true);
        
        btnEndavant = new Button();
        btnEndavant.setText("-->");
        btnEndavant.setDisable(true);
        
        lbl = new Label();
        lbl.setText("");
        lbl.setPrefHeight(80);
        
        btn.setOnAction(event -> presenter.calcula(textField.getText().toString(), true));
        btnEnrrera.setOnAction(event -> presenter.enrrera());
        btnEndavant.setOnAction(event -> presenter.endavant());
        
        VBox root = new VBox();
        root.setAlignment(Pos.CENTER);
        
        root.getChildren().addAll(textField, btn, btnEnrrera, btnEndavant,lbl);
        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();
		
	}

}
