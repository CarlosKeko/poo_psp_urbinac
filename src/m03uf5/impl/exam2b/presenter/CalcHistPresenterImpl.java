package m03uf5.impl.exam2b.presenter;

import java.util.ArrayList;

import m03uf5.impl.exam2b.CalcHistContract.CalcHistPresenter;
import m03uf5.impl.exam2b.CalcHistContract.CalcHistView;
import m03uf5.impl.exam2b.model.CalcHistModelImpl;

public class CalcHistPresenterImpl implements CalcHistPresenter{

	private CalcHistView view;
	private ArrayList<String> historicCalcul = new ArrayList<String>();
	private int posicioActual = 0;

	public void setView(CalcHistView v) {
		this.view = v;
		
	}

	public void calcula(String expressio, boolean nou) {
		CalcHistModelImpl calcul = new CalcHistModelImpl();
		
		try {
			String auxResultat = String.valueOf(calcul.calcular(expressio)); //Creo esta variable para no tener que hacer dos veces el calculo y hacer que el programa haga menos vueltas.
			view.mostra(auxResultat);
			
			if (nou) {
				historicCalcul.add(expressio);
				posicioActual = historicCalcul.size() - 1;
				
				if (this.posicioActual >= 1) {
					this.view.deshabilitarEndavant();
					this.view.habilitarEnrrera();
				}
			}
			
		}catch (Exception e) {
			view.mostra("Error caracters invalids");
			
		}
	}

	public void enrrera() {
		if (this.posicioActual > 0) {
			this.posicioActual = this.posicioActual - 1;
			view.modificarTextField(historicCalcul.get(this.posicioActual));
			this.calcula(this.historicCalcul.get(this.posicioActual), false);
			
			if (this.posicioActual > 0) {
				view.habilitarEnrrera();
				
			}else {
				view.deshabilitarEnrrera();
			}
			
			if (this.posicioActual < this.historicCalcul.size() - 1) {
				view.habilitarEndavant();
			}
			
		}
	}

	public void endavant() {
		if (this.posicioActual < this.historicCalcul.size() - 1) {
			this.posicioActual = this.posicioActual + 1;
			view.modificarTextField(historicCalcul.get(this.posicioActual));
			this.calcula(this.historicCalcul.get(this.posicioActual), false);
			
			if (this.posicioActual < this.historicCalcul.size() - 1) {
				view.habilitarEndavant();
				
			}else {
				view.deshabilitarEndavant();
			}
			
			if (this.posicioActual < this.historicCalcul.size() - 1 && this.posicioActual > 0) {
				view.habilitarEnrrera();
			}
			
		}
	}

}
