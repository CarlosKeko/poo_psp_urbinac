package m03uf5.impl.exam2b.model;

import m03uf5.prob.calcbox.ExprAvaluator;

public class CalcHistModelImpl {
	
	public double calcular(String calcul) throws Exception{	
		ExprAvaluator calcular = new ExprAvaluator();
		return calcular.avalua(calcul);
		
	}
	
}
