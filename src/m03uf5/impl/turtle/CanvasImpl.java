package m03uf5.impl.turtle;

import lib.StdDraw;
import m03uf5.prob.turtle.Canvas;
import m03uf5.prob.turtle.Color;

public class CanvasImpl implements Canvas{

	@Override
	public void setScale(double scale) {
		// TODO Auto-generated method stub
		StdDraw.setScale(0, scale);
		
	}

	@Override
	public double getWidth() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getHeight() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setColor(Color color) {
		// TODO Auto-generated method stub
		switch (color) {
			case BLACK: StdDraw.setPenColor(StdDraw.BLACK);
				break;
			
			case YELLOW: StdDraw.setPenColor(StdDraw.YELLOW);
				break;
			
			case RED: StdDraw.setPenColor(StdDraw.RED);
				break;
			
			case BLUE: StdDraw.setPenColor(StdDraw.BLUE);
				break;
				
			case GREEN: StdDraw.setPenColor(StdDraw.GREEN);
				break;
			
		}
		
		
	}

	@Override
	public void setPenSize(double size) {
		// TODO Auto-generated method stub
		StdDraw.setPenRadius(size);
		
	}

	@Override
	public void drawPoint(double x, double y) {
		// TODO Auto-generated method stub
		StdDraw.point(x, y);
		
	}

	@Override
	public void drawLine(double x0, double y0, double x1, double y1) {
		// TODO Auto-generated method stub
		StdDraw.line(x0, y0, x1, y1);
		
	}

	@Override
	public void save(String filename) {
		// TODO Auto-generated method stub
		StdDraw.save(filename);
		
	}

}
