package m03uf5.impl.turtle.run;

import lib.Problems;
import lib.Reflections;
import m03uf5.impl.turtle.FlocNeuFigure;
import m03uf5.prob.turtle.Canvas;

/**
 * @version 1.0
 * @author Carlos Urbina
 *
 */

public class FlocNeuMain {

	public static void main(String[] args) {
		
		String packageName = Problems.getImplPackage(Canvas.class);
		Canvas canvas = Reflections.newInstanceOfType(Canvas.class, packageName + ".CanvasImpl");
		FlocNeuFigure fl = new FlocNeuFigure();
		
		
		canvas.setScale(100);
		canvas.setPenSize(0.002);
		fl.f.setCanvas(canvas);
		fl.draw(fl.f);
		
		
	}

}
