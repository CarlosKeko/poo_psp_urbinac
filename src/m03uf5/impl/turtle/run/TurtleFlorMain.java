package m03uf5.impl.turtle.run;

import lib.Problems;
import lib.Reflections;
import m03uf5.prob.turtle.Canvas;
import m03uf5.prob.turtle.Turtle;

/**
 * @version 1.0
 * @author Carlos Urbina
 *
 */

public class TurtleFlorMain {

	public static void main(String[] args) {
		
		String packageName = Problems.getImplPackage(Canvas.class);		
		Canvas canvas = Reflections.newInstanceOfType(Canvas.class, packageName + ".CanvasImpl");
		Turtle turtle = Reflections.newInstanceOfType(Turtle.class, packageName + ".TurtleImpl");
		
		canvas.setScale(100);
		canvas.setPenSize(0.002);
		turtle.setCanvas(canvas);
		
		turtle.up();
		turtle.goForward(50);
		turtle.turnLeft(90);
		turtle.down();
		
		draw(turtle, 25, 5);
	}
	
	static void draw(Turtle t, double branca, double petal) {
		
		t.goForward(branca);
		
		t.turnLeft(45);
		t.goForward(petal);
		t.turnRight(90);
		t.goForward(petal);
		t.turnRight(90);
		t.goForward(petal);
		t.turnRight(90);
		t.goForward(petal);
		t.turnLeft(45);
		
		t.goForward(branca);			
		t.turnLeft(180);
	}
}
