 package m03uf5.impl.turtle;

import m03uf5.prob.turtle.Canvas;
import m03uf5.prob.turtle.Turtle;

/**
 * @version 1.0
 * @author Carlos Urbina
 *
 */

public class TurtleImpl implements Turtle{

	public Canvas canvasTurtle;
	public boolean posicio = true;	//Si es true esta baixada i si es false esta pujada.
	public double delta = 0;
	public double posiciox = 0;
	public double posicioy = 0;   //Controla la direccion
	public double posicioypuntb = 0;	//Controla la direccion
	public double angle = 0;
	
	public void up() {
		System.out.println("Tortuga pujada\n");
		this.posicio = false;
		
	}

	public void down() {
		System.out.println("Tortuga baixada\n");
		this.posicio = true;
		
	}

	public double x() {
		return this.posiciox;
	}

	public double y() {
		return this.posicioy;
	}

	public double angle() {
		return this.angle;
	}

	public void turnLeft(double delta) {
		if (this.angle + delta <= 360) {
			this.angle = this.angle + delta;
			 
		}else {
			this.angle = (this.angle + delta) - 360;
			
		}
	}

	public void turnRight(double delta) {
		if (this.angle - delta >= 0) {
			this.angle = this.angle - delta;
			
		}else {
			this.angle = (this.angle - delta) + 360;
		}
	}

	public void goForward(double step) {
		this.posicioypuntb = this.posicioypuntb + step * Math.sin(Math.toRadians(this.angle()));
		step = this.x() + step * Math.cos(Math.toRadians(this.angle()));
		if (this.posicio) {
			
			
			

			
			
			this.getCanvas().drawLine(this.posiciox, this.posicioy, step, this.posicioypuntb);
	
		}else {
			System.out.println("No es pot dibuixar perque la tortuga esta pujada\n");
		}
		
		this.posiciox = step;
		this.posicioy = this.posicioypuntb;
		

		
	}

	@Override
	public void goBackward(double step) {
		// TODO Auto-generated method stub
		if (this.posicio) {
			this.getCanvas().drawLine(step, step, step, step);
			
		}else {
			System.out.println("La tortuga esta pujada");
		}
		
		
	}

	@Override
	public void setCanvas(Canvas canvas) {
		// TODO Auto-generated method stub
		this.canvasTurtle = canvas;
		
	}

	@Override
	public Canvas getCanvas() {
		// TODO Auto-generated method stub
		return canvasTurtle;
	}

}
