package m03uf5.impl.turtle;

import m03uf5.prob.turtle.Figure;
import m03uf5.prob.turtle.Turtle;

/**
 * @version 1.0
 * @author Carlos Urbina
 *
 */

public class FlorFigure implements Figure {
	
	private static double branca = 25;
	private static double petal = 5;
	
	public void draw(Turtle t) {
		
		t.goForward(branca);
		
		t.turnLeft(45);
		t.goForward(petal);
		t.turnRight(90);
		t.goForward(petal);
		t.turnRight(90);
		t.goForward(petal);
		t.turnRight(90);
		t.goForward(petal);
		t.turnLeft(45);
		
		t.goForward(branca);			
		t.turnLeft(180);
			
	}
}


