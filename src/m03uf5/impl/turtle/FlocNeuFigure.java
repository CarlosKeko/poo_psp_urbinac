package m03uf5.impl.turtle;

import m03uf5.prob.turtle.Figure;
import m03uf5.prob.turtle.Turtle;

/**
 * @version 1.0
 * @author Carlos Urbina
 *
 */

public class FlocNeuFigure implements Figure{
	
	public TurtleImpl f = new TurtleImpl();
	public FlorFigure dibuixar = new FlorFigure();
	
	public void draw(Turtle t) {
		t.up();
		t.goForward(50);
		t.turnLeft(90);
		t.goForward(50);
		t.down();
		dibuixar.draw(t);
		t.turnLeft(180);
		dibuixar.draw(t);
		t.turnLeft(90);
		dibuixar.draw(t);
		t.turnLeft(45);
		dibuixar.draw(t);
		t.turnLeft(135);
		dibuixar.draw(t);
		t.turnLeft(135);
		dibuixar.draw(t);
		t.turnLeft(270);
		dibuixar.draw(t);
		t.turnLeft(270);
		dibuixar.draw(t);
		
	}
}
