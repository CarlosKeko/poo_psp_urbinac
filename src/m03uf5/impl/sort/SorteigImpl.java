package m03uf5.impl.sort;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import m03uf5.prob.sort.GeneradorBolets;
import m03uf5.prob.sort.GeneradorPremis;
import m03uf5.prob.sort.Sorteig;

public class SorteigImpl<K, V> implements Sorteig<K, V>{

	private Queue<K> concursants = new LinkedList<K>();
	private int numPremis;
	private GeneradorPremis<V> genPremis;
	private GeneradorBolets<K, V> genBolets;
	private HashMap<V, Boolean> mapaPremis = new HashMap<V, Boolean>();
	
	public void setNumPremis(int numPremis) {
		this.numPremis = numPremis;
		
	}

	public void setGenPremis(GeneradorPremis<V> genpremis) {
		this.genPremis = genpremis;
		
	}

	public void setGenBolets(GeneradorBolets<K, V> genbolets) {
		this.genBolets = genbolets;
		
	}

	public V afegir(K concursant) {
		this.concursants.offer(concursant);
		return this.genBolets.genera(concursant);
	}

	public Queue<K> concursants() {
		return this.concursants;
	}

	public Set<K> sortejar() {
		int numConcursants = this.concursants.size();
		Set<K> guanyadors = new HashSet<K>();
		K concursant;
		Set<V> premis = new HashSet<V>();
		
		while (premis.size() < this.numPremis) {
			premis.add(genPremis.genera());
		}
		
		for (V v : premis) {
			this.mapaPremis.put(v, false);
		}
		
		for (int i = 0; i < numConcursants; i++) {
			concursant = this.concursants.poll();
			for (V key : this.mapaPremis.keySet()) {
				if (this.genBolets.genera(concursant) == key && !this.mapaPremis.get(key)) {
					this.mapaPremis.put(key, true);
					guanyadors.add(concursant);
				}
			}
		}
		
		return guanyadors;
	}
	
	public Map<V, Boolean> premiats() {
		return this.mapaPremis;
	}

}
