package m03uf5.impl.penjat.presenter;

import m03uf5.impl.penjat.model.PenjatModel;
import m03uf5.impl.penjat.view.PenjatView;

public class PenjatPresenter {
	
	private PenjatView view;
	private PenjatModel model;
	
	public void setView(PenjatView view) {
		this.view = view;
		
	}
	
	public void setModel(PenjatModel model) {
		this.model = model;
		
	}
	
	public void iniciar() {
		this.model.iniciarJoc();
		this.view.mostra(this.model.getParaulaSecreta());
		this.view.mostraTorn(this.model.getTorn());
	}
	
	public void jugar(char lletra) {
		try {
			int trobades = this.model.jugar(lletra);
			this.view.mostraTrobades(trobades);
			this.view.mostra(this.model.getParaulaSecreta());
			
			this.view.mostraLletres(this.model.getLletresGastades());
			
			if (trobades <= 0) {
				this.view.mostraTorn(this.model.getTorn());
			}
				
			if (model.comptarLletresAmagades() <= 0) {
				this.view.mostrarGuanyades(this.model.obtenirPartidesGuanyades());
				this.model.setTorn("");
				this.model.reiniciarLletresGastades();
				this.view.fi();
				
			}
			
		}catch (Exception ex) {
			System.out.println(ex);
		}

	}

}
