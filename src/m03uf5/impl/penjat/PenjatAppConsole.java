package m03uf5.impl.penjat;

import java.util.Scanner;

import m03uf5.impl.penjat.model.PenjatModel;

public class PenjatAppConsole {
	
	public static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {	
		PenjatModel penjat = new PenjatModel();
		penjat.iniciarJoc();
		char c;
		int trobades;
		String torn = penjat.getTorn();
		
		while (penjat.comptarLletresAmagades() > 0) {
			try {
				System.out.println(penjat.getParaulaSecreta());
				System.out.println("Torn de " + torn);
				c = sc.next().charAt(0);
				trobades = penjat.jugar(c);		
				
				if (trobades == 0) {
					torn = penjat.getTorn();
				}
				
				System.out.println("Has descobert " + trobades + " lletres");
			}catch (Exception ex) {
				System.out.println("Error " + ex);
			}

			
		}
		
		System.out.println(penjat.getParaulaSecreta());
		System.out.println("\nHa guanyat " + torn);
	}
}
