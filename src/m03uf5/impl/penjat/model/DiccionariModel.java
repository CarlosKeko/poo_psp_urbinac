package m03uf5.impl.penjat.model;

import java.util.ArrayList;
import java.util.Random;

public class DiccionariModel {
	
	ArrayList<String> llistaParaules = new ArrayList<String>();
	
	public DiccionariModel() {
		llistaParaules.add("Biling�e");
		llistaParaules.add("Arbol");
		llistaParaules.add("Buenos d�as");
		llistaParaules.add("Espa�a");
		llistaParaules.add("J�rasico");
		
	}
	
	public String demanarParaula() {
		Random r = new Random();	
		return this.llistaParaules.get(r.nextInt(this.llistaParaules.size()));
	}
	
}
