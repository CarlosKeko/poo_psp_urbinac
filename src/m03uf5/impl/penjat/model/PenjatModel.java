package m03uf5.impl.penjat.model;

import java.text.Normalizer;
import java.util.ArrayList;

public class PenjatModel {
	
	private String paraula = "";
	private String  paraulaSecreta = "";
	private String torn = "";
	private ArrayList<String> lletresGastades = new ArrayList<String>();
	private int guanyades1 = 0;
	private int guanyades2 = 0;
	
	public void setParaula(String paraula) {
		this.paraula = paraula;
	}
	
	public void setParaulaSecreta(String paraulSecreta) {
		this.paraulaSecreta = paraulSecreta;
	}
	
	public String getParaulaSecreta() {
		return this.paraulaSecreta;
	}
	
	public void setTorn(String torn) {
		this.torn = torn;
	}
	
	public String getTorn() {	
		if (this.torn.equalsIgnoreCase("Jugador 2") || this.torn.equalsIgnoreCase("")) {
			this.torn = "Jugador 1";
			
		}else {
			this.torn = "Jugador 2";
		}
		
		return this.torn;
	}
	
	public String obtenirPartidesGuanyades() {
		if (this.torn.equalsIgnoreCase("Jugador 1")) {
			this.guanyades1++;

		}else {
			this.guanyades2++;

		}
		return "      Partides guanyades:\nJugador 1: " + this.guanyades1 + "\tJugador 2: " + this.guanyades2;
	}
	
	public ArrayList<String> getLletresGastades() {
		return this.lletresGastades;
	}
	
	public void reiniciarLletresGastades() {
		this.lletresGastades = new ArrayList<String>();
	}
	
	public int jugar(char c) {
		int trobades = 0;
		
		String aux = "";
		
		if (Character.isLetter(c) && !this.lletresGastades.contains(Character.toString(this.normalitzar(Character.toString(c)).charAt(0)).toUpperCase())) {
			this.lletresGastades.add(this.normalitzar(Character.toString(c)).toUpperCase());
		}
		
		for (int i = 0; i < this.paraula.length(); i++) {
			if (this.normalitzar(Character.toString(c)).toUpperCase().charAt(0) == this.normalitzar(this.paraula).toUpperCase().charAt(i) && this.paraulaSecreta.charAt(i) == '_') {
				trobades++;
				aux = aux  + this.paraula.charAt(i);
				
			}else {
				aux = aux + this.paraulaSecreta.charAt(i);
			}
			
		}
		
		this.paraulaSecreta = aux;
		
		return trobades;
	}
	
	public void iniciarJoc() {
		DiccionariModel diccionari = new DiccionariModel();
		String paraulaOriginal = diccionari.demanarParaula();
		this.paraulaSecreta = "";
		this.paraula = "";
		
		for (int i = 0; i < paraulaOriginal.length(); i++) {
			if (paraulaOriginal.charAt(i) == ' ') {
				this.paraulaSecreta = this.paraulaSecreta + "   ";
				this.paraula = this.paraula + "   ";
				
			}
			else {
				this.paraulaSecreta = paraulaSecreta + " _ ";
				this.paraula = this.paraula + " " + paraulaOriginal.charAt(i) + " ";
			}

		}
		
	}
	
	public String normalitzar(String valor) {	
    	String text = Normalizer.normalize(valor, Normalizer.Form.NFD);
    	text = text.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
    	
    	return text;
	}
	
	
	public int comptarLletresAmagades() {
		int numLletresAmagades = 0;
		
		for (int i = 0; i < paraulaSecreta.length(); i++) {
			if (paraulaSecreta.charAt(i) == '_') {
				numLletresAmagades++;
			}
		}
		
		return numLletresAmagades;
		
	}

}
