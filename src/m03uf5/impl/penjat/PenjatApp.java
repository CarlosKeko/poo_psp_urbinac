package m03uf5.impl.penjat;

import javafx.application.Application;
import javafx.stage.Stage;
import m03uf5.impl.penjat.model.PenjatModel;
import m03uf5.impl.penjat.presenter.PenjatPresenter;
import m03uf5.impl.penjat.view.PenjatView;

public class PenjatApp extends Application  {

	private PenjatView v;
	private PenjatPresenter p;
	private PenjatModel m;
	
	public void start(Stage primaryStage) throws Exception {
		p = new PenjatPresenter();
		v = new PenjatView(primaryStage);
		m = new PenjatModel();
		
		p.setView(v);
		p.setModel(m);
		v.setPresenter(p);
	}
	
	public static void main(String[] args) {
		launch(args);
		
	}

}
