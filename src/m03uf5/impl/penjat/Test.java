package m03uf5.impl.penjat;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import m03uf5.impl.penjat.model.PenjatModel;

class Test {

	@org.junit.jupiter.api.Test
	void testModel1() {
		int n;
		
		PenjatModel model = new PenjatModel();
		
		model.setParaula(" H o l a ");
		model.setParaulaSecreta(" _ _ _ _ ");
		
		assertEquals(4, model.comptarLletresAmagades());
		
		n = model.jugar('h');
		assertEquals(1, n);

		assertEquals(3, model.comptarLletresAmagades());

		n = model.jugar('o');
		assertEquals(1, n);
		
		assertEquals(2, model.comptarLletresAmagades());
		
		n = model.jugar('l');
		assertEquals(1, n);
		
		assertEquals(1, model.comptarLletresAmagades());
		
		n = model.jugar('a');
		assertEquals(1, n);
		
		assertEquals(0, model.comptarLletresAmagades());
		
		String torn = model.getTorn();
		assertTrue(torn.equals("Jugador 1") || torn.equals("Jugador 2"));
		
		String partidesGuanyades = model.obtenirPartidesGuanyades();
		assertTrue(partidesGuanyades.equals("      Partides guanyades:\nJugador 1: " + 1 + "\tJugador 2: " + 0));
	}
	
	@org.junit.jupiter.api.Test
	void testModel2() {
		PenjatModel model = new PenjatModel();
		model.iniciarJoc();
		assertTrue(model.getParaulaSecreta().length() > 0);
		
		String torn = model.getTorn();
		assertTrue(torn.equals("Jugador 1") || torn.equals("Jugador 2"));
		
		String partidesGuanyades = model.obtenirPartidesGuanyades();
		assertTrue(partidesGuanyades.equals("      Partides guanyades:\nJugador 1: " + 1 + "\tJugador 2: " + 0));
		
		model.setParaula(" H o l a ");
		model.setParaulaSecreta(" _ _ _ _ ");
		
		assertEquals(4, model.comptarLletresAmagades());
		
		int n = model.jugar('h');
		assertEquals(1, n);
		
		assertEquals(" H _ _ _ ", model.getParaulaSecreta());
		
		ArrayList<String> prueba = new ArrayList<String>();
		prueba.add("H");
		
		assertEquals(prueba, model.getLletresGastades());
		
		assertEquals("a", model.normalitzar("�"));
	}

}
