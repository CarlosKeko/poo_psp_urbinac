package m03uf5.impl.penjat.view;

import java.util.ArrayList;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import m03uf5.impl.penjat.presenter.PenjatPresenter;

public class PenjatView {
	
	private PenjatPresenter presenter;
	private Stage primaryStage;
	private Label lbl;
	private Label lblTorn;
	private Label lblTrobades;
	private Label lblLletres;
	private Label lblPartidasGuanyades;
	
	public PenjatView(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.inici(primaryStage);
		
	}
	
	public Stage getStage() {
		return this.primaryStage;
	}
	
	public void setPresenter(PenjatPresenter presenter) {
		this.presenter = presenter;
	}
	
	public void mostra(String resultat) {
		this.lbl.setText(resultat);
	}
	
	public void mostraTorn(String torn) {
		this.lblTorn.setText(torn);
	}
	
	public void mostraTrobades(int trobades) {
		this.lblTrobades.setText("Trobades: " + Integer.toString(trobades));
	}
	
	public void mostraLletres(ArrayList<String> arrayList) {
		this.lblLletres.setText("Lletres fetes servir:" + arrayList);
	}
	
	public void mostrarGuanyades(String guanyades) {
		this.lblPartidasGuanyades = new Label();
		this.lblPartidasGuanyades.setText(guanyades);
	}
	
	public void iniciar() {
		this.presenter.iniciar();
		
	}
	
	public void fi() {
		VBox root = new VBox();
        root.setAlignment(Pos.CENTER);
        
        Label lblParaula = new Label();
        lblParaula.setText("La paraula era: " + this.lbl.getText().toString());
        lblParaula.setPrefHeight(50);
        
        Label lblGuanyador = new Label();
        lblGuanyador.setText("Guanyador: " + this.lblTorn.getText().toString());
        lblGuanyador.setPrefHeight(40);
        
        Button btnTornarAJugar = new Button();
        btnTornarAJugar.setText("�Tornar a jugar?");
        btnTornarAJugar.setPrefHeight(80);
        btnTornarAJugar.setPrefWidth(250);
        
        btnTornarAJugar.setOnAction(event -> {
        	this.inici(this.getStage());
        });
        
        root.getChildren().addAll(this.lblPartidasGuanyades, lblParaula, lblGuanyador, btnTornarAJugar);
        this.getStage().setScene(new Scene(root, 300, 250)); 
	}
	
	public void inici (Stage primaryStage) {
        VBox root = new VBox();
        root.setAlignment(Pos.CENTER);
		primaryStage.setTitle("Penjat");
		
        Button btnIniciarJoc = new Button();
        btnIniciarJoc.setText("Iniciar joc");
        btnIniciarJoc.setPrefHeight(80);
        btnIniciarJoc.setPrefWidth(80);
        
        Label lblTitulo = new Label();
        lblTitulo.setText("PENJAT");
        lblTitulo.setPrefHeight(80);
        
        btnIniciarJoc.setOnAction(event -> {
        	this.joc();
        });
        
        root.getChildren().addAll(lblTitulo, btnIniciarJoc);
        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();
		
	}
	
	public void joc() {
		VBox root = new VBox();
        root.setAlignment(Pos.CENTER);
        
        Button btnJugar = new Button();
        btnJugar.setText("Jugar");
        btnJugar.setPrefHeight(50);
        btnJugar.setPrefWidth(50);
		
        this.lblLletres = new Label();
        this.lblLletres.setText("Lletres fetes servir: ");
        this.lblLletres.setPrefHeight(80);
        
        this.lbl = new Label();
        this.lbl.setText("");
        this. lbl.setPrefHeight(80);
        
        this.lblTorn = new Label();
        
        this.lblTrobades = new Label();
        this.lblTrobades.setText("Trobades: ");
        this.lblTrobades.setPrefHeight(50);
        
        TextField textField = new TextField();
        textField.setPrefHeight(50);
        textField.setPrefWidth(10);
        addTextLimiter(textField, 1);
        
        btnJugar.setOnAction(event -> {
        	if (textField.getText().toString().length() > 0) {
        		this.presenter.jugar(textField.getText().toString().charAt(0));
            	textField.setText("");
        	}
        });
        
    	this.iniciar();
        root.getChildren().addAll(this.lblTorn, this.lbl, textField, btnJugar, this.lblTrobades, this.lblLletres);
        this.getStage().setScene(new Scene(root, 300, 250)); 
        this.getStage().show();
	}
	
	//He cogido este metodo de: https://stackoverrun.com/es/q/4110610
	public static void addTextLimiter(final TextField tf, final int maxLength) {
	       tf.textProperty().addListener(new ChangeListener<String>() {
	           @Override
	           public void changed(final ObservableValue<? extends String> ov, final String oldValue, final String newValue) {
	               if (tf.getText().length() > maxLength) {
	                   String s = tf.getText().substring(0, maxLength);
	                   tf.setText(s);
	               }
	           }
	       });
	}
	
}
