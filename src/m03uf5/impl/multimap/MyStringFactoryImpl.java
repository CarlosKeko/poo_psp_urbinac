package m03uf5.impl.multimap;

import m03uf5.prob.multimap.MyString;
import m03uf5.prob.multimap.MyStringFactory;

public class MyStringFactoryImpl implements MyStringFactory{

	private MyStringImpl frase;

	public MyString create(String value) {
		this.frase = new MyStringImpl(value);
		return frase;
	}

}
