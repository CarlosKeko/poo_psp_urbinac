package m03uf5.impl.multimap.test;

import java.util.Collection;
import java.util.HashSet;

import lib.Problems;
import lib.Reflections;
import m03uf5.impl.multimap.MyStringFactoryImpl;
import m03uf5.prob.multimap.MultiMap;
import m03uf5.prob.multimap.MyString;
import m03uf5.prob.multimap.MyStringFactory;

public class prueba {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyStringFactoryImpl frase = new MyStringFactoryImpl();
		
		MyString frase1 = frase.create("hola");
		MyString frase2 = frase.create("hola");
		
		
		System.out.println(frase1.equals(frase2));
		
//		MultiMapListImpl jaja = new MultiMapListImpl();
		
		
		
		String packageName = Problems.getImplPackage(MultiMap.class);
		
		MyStringFactory msf = Reflections.newInstanceOfType(
				MyStringFactory.class, packageName + ".MyStringFactoryImpl");	
		
		@SuppressWarnings("unchecked")
		MultiMap<Integer, MyString> mmap = Reflections.newInstanceOfType(
				MultiMap.class, packageName + ".MultiMapListImpl");
		
		mmap.put(1, msf.create("U"));
		mmap.put(1, msf.create("Uno"));
		mmap.put(2, msf.create("Dos"));
		mmap.put(2, msf.create("Zero"));
		mmap.put(2, msf.create("Dos"));
		mmap.put(3, msf.create("Tres"));
		
		mmap.get(3).add(msf.create("Three"));
		mmap.get(4).add(msf.create("Zero"));
		mmap.get(5).add(msf.create("Cinc"));
	
		System.out.println(mmap.remove(1, msf.create("Uno")));
		
		System.out.println(mmap.size());
		System.out.println(mmap.get(5).isEmpty());
		
		System.out.println(mmap.size());
		
		Collection<String> keys = new HashSet<String>();
		keys.add("1");
		keys.add("2");
		keys.add("3");
		keys.add("4");
		keys.add("5");
		
		for (String b : keys) {
			System.out.println(b);
		}
		
	}

}
