package m03uf5.impl.multimap;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import m03uf5.prob.multimap.MultiMap;

public class MultiMapSetImpl<K, V> implements MultiMap<K, V> {

	private Map<K, HashSet<V>> multiMap = new HashMap<K, HashSet<V>>();
	
	public Iterator<K> iterator() {
		Collection<K> keys = new HashSet<K>();
		
		for (K values : this.multiMap.keySet()) {
			keys.add(values);
		}
		
		Iterator<K> mapa = keys.iterator();
		
		return mapa;
	}

	public int size() {
		return this.multiMap.size();
	}

	public void put(K key, V value) {
		HashSet<V> aux = new HashSet<V>();
		aux = (HashSet<V>) this.get(key);
		aux.add(value);
		this.multiMap.put(key, aux);
	}

	public Collection<V> get(K key) {
		Collection<V> values = new HashSet<V>();
		boolean encontrat = false;
		HashSet<V> aux = new HashSet<V>();
		
		for (Entry<K, HashSet<V>> entry : this.multiMap.entrySet()) {
			if (key == entry.getKey()) {
				values = entry.getValue();
				encontrat = true;
				
			}	
		}
		
		if (!encontrat) {
			this.multiMap.put(key, aux);
			for (Entry<K, HashSet<V>> entry : this.multiMap.entrySet()) {
				if (key == entry.getKey()) {
					values = entry.getValue();
					
				}	
			}
		}
		
		return values;
	}

	public boolean remove(K key, V value) {
		return this.get(key).remove(value);
	}

}
