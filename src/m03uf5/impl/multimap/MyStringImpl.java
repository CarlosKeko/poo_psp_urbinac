package m03uf5.impl.multimap;

import m03uf5.prob.multimap.MyString;

public class MyStringImpl implements MyString{

	private String frase;
	
	//CONSTRUCTORS
	
	public MyStringImpl(String value) {
		this.frase = value;
	}
	
	
	//GETTERS I SETTERS
	
	public String get() {
		return this.frase;
	}

	public void set(String value) {
		this.frase = value;
		
	}
	
	
	//METODES
	
	public String toString() {
		return this.frase;
		
	}
	
	public boolean equals(Object v) {
		MyStringImpl value = (MyStringImpl) v;
		
		if (this.get().equals(value.get())) {
			return true;
			
		}else {
			return false;
		}
	}
	
	public int hashCode() {
		return this.get().hashCode();
		
	}

}
