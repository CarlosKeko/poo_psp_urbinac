//package m03uf5.impl.arbreg;
//
//import java.util.ArrayList;
//import java.util.Collection;
//
//import m03uf5.prob.arbreg.Parella;
//import m03uf5.prob.arbreg.Persona;
//import m03uf5.prob.arbreg.Sexe;
//
//public class PersonaImpl1 implements Persona{
//
//	private String nom;
//	private Sexe sexe;
//	private Parella<Persona, Persona> pares;
//	private Collection<Persona> fills = new ArrayList<>();
//	
//	//CONSTRUCTORS
//	public PersonaImpl1() {
//		this.nom = "Default"; 
//		this.sexe = Sexe.F;
//		this.pares = null;
//	}
//	
//	public PersonaImpl1(String n, Sexe s, Parella<Persona, Persona> p) {
//		this.nom = n;
//		this.sexe = s;
//		this.pares = p;
//	}
//	
//	//GETTERS
//	public String getNom() {
//		return this.nom;
//	}
//
//	public Sexe getSexe() {
//		return this.sexe;
//	}
//
//	public Parella<Persona, Persona> getPares() {
//		return this.pares;
//	}
//
//	public Collection<Persona> getFills() {
//		if (this.getSexe() == Sexe.F) {
//			for (Persona p : ArbreGenImpl.getArbre()) {
//				if (p.getPares().getDreta() == this) {
//					this.fills.add(p);
//				}
//			}
//			
//		}else if (this.getSexe() == Sexe.M) {		
//			for (Persona p : ArbreGenImpl.getArbre()) {	
//				if (p.getPares().getEsquerra() == this) {
//					this.fills.add(p);
//				}
//			}
//			
//		}else {
//			System.out.println("El sexe de la persona no esta definit");
//			
//		}
//		return this.fills;
//	}
//	
//	public String toString() {
//		return this.getNom();
//	}
//
//}
