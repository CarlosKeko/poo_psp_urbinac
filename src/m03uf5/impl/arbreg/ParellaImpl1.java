//package m03uf5.impl.arbreg;
//
//import m03uf5.prob.arbreg.Parella;
//
//public class ParellaImpl1<E, D> implements Parella<E, D> {
//
//	private E e;
//	private D d;
//	
//	public ParellaImpl1(E es, D dr) {
//		this.e = es;
//		this.d = dr;
//	}
//	
//	public E getEsquerra() {
//		return this.e;
//	}
//
//	public D getDreta() {
//		return this.d;
//	}
//	
//	public String toString() {
//		return "{E=" + this.getEsquerra() + ", D=" + this.getDreta() + "}"; 
//	}
//
//}
