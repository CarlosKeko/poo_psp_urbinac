//package m03uf5.impl.arbreg;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.HashSet;
//import java.util.Iterator;
//
//import lib.db.DuplicateException;
//import m03uf5.prob.arbreg.ArbreGen;
//import m03uf5.prob.arbreg.Parella;
//import m03uf5.prob.arbreg.Persona;
//import m03uf5.prob.arbreg.Sexe;
//import m03uf5.prob.arbregex.ArbreGenEx;
//
//public class ArbreGenImpl1 implements ArbreGen{
//
//	
//	private static Collection<Persona> arbre = new HashSet<>();
//	private Collection<Parella> parelles = new ArrayList<>();
//	private Collection<Persona> parellesPersona = new ArrayList<>();
//	private Parella<Persona, Persona> parella;
//	private Persona persona;
//	
//	public Iterator<Persona> iterator() {
//		Iterator<Persona> persones = arbre.iterator();
//		return persones;
//	}
//	
//	public Parella<Persona, Persona> createPares(Persona pare, Persona mare) {
//		this.parella = new ParellaImpl<Persona, Persona>(pare, mare);
//		this.parelles.add(this.parella);
//		return this.parella;
//	}
//
//	public Persona addPersona(String nom, Sexe sexe, Parella<Persona, Persona> pares) {
//		persona = new PersonaImpl(nom, sexe, pares);
//		arbre.add(this.persona);
//		return persona;
//	}
//
//	public Persona get(String nom) {
//		for (Persona p : arbre) {
//			if (p.getNom().equalsIgnoreCase(nom)) {
//				return p;
//			}
//		}
//		return null;
//	}
//
//	public Collection<Persona> getParelles(Persona p) {
//		
//		this.parellesPersona = new ArrayList<>();
//		boolean repetit = false;
//		
//		if (p.getSexe() == Sexe.M) {
//			 for (Parella<?, ?> pa : this.parelles) {
//				 repetit = false;
//				 
//				 if (pa.getEsquerra() == p) {
//					 for (Persona perso : parellesPersona) {
//						 if (pa.getDreta() == perso) {
//							 repetit = true;
//						 }
//					 }
//					 
//					 if (!repetit) {
//						 this.parellesPersona.add((Persona) pa.getDreta());
//					 }	 
//				 }
//			 }
//			
//		}else if (p.getSexe() == Sexe.F) {
//			for (Parella<?, ?> pa : this.parelles) {
//				repetit = false;
//				
//				if (pa.getDreta() == p) {
//					 for (Persona perso : parellesPersona) {
//						 if (pa.getEsquerra() == perso) {
//							 repetit = true;
//						 }
//					 }
//					
//					if (!repetit) {
//						this.parellesPersona.add((Persona) pa.getEsquerra());
//					}
//				}
//			}
//		}else {
//			System.out.println("Sexe no definit");
//		}
//		
//		return this.parellesPersona;
//	}
//
//	public Collection<Persona> getFillsEnComu(Persona pare, Persona mare) {
//		Collection<Persona> fills = new ArrayList<>();
//		
//		for (Persona p : arbre) {
//			if (p.getPares().getEsquerra() == pare && p.getPares().getDreta() == mare) {
//				fills.add(p);
//			}
//		}
//		return fills;
//	}
//	
//	public static Collection<Persona> getArbre() {
//		return arbre;
//	}
//
//}
