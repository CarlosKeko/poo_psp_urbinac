package m03uf5.impl.arbregex;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import lib.db.DuplicateException;
import lib.db.NotFoundException;
import m03uf5.impl.arbreg.ParellaImpl;
import m03uf5.impl.arbreg.PersonaImpl;
import m03uf5.prob.arbreg.Parella;
import m03uf5.prob.arbreg.Persona;
import m03uf5.prob.arbreg.Sexe;
import m03uf5.prob.arbregex.ArbreGenEx;

public class ArbreGenExImpl implements ArbreGenEx{

	private Map<String, Persona> persones;
	
	public ArbreGenExImpl() {
		persones = new LinkedHashMap<>(); // keep insert order
	}
	
	public Iterator<Persona> iterator() {
		return persones.values().iterator();
	}

	public Parella<Persona, Persona> createPares(Persona pare, Persona mare) {
		return new ParellaImpl<Persona, Persona>(pare, mare);
	}

	public Persona addPersona(String nom, Sexe sexe, Parella<Persona, Persona> pares) throws DuplicateException, IllegalArgumentException {
		
		if (nom == null || sexe == null || pares == null) {
			throw new IllegalArgumentException(nom + "," + sexe + "," + pares);
			
		}else if (persones.containsKey(nom)){
			throw new DuplicateException(nom);
			
		}else {
			Persona p = new PersonaImpl(nom, sexe, pares);
			persones.put(nom, p);
			return p;
		}
		

	}

	public Persona get(String nom) throws NotFoundException, IllegalArgumentException {
		if (nom == null) {
			throw new IllegalArgumentException(nom);
			
		}else if (persones.get(nom) == null) {
			throw new NotFoundException(nom);
			
		}else {
			return persones.get(nom);
			
		}

	}

	public String toString() {
		return persones.values().toString();
	}
	
	public Collection<Persona> getParelles(Persona p) throws IllegalArgumentException {
		if (p == null) {
			throw new IllegalArgumentException();
			
		}else {
			Set<Persona> result = new HashSet<>();	
			Collection<Persona> fills = p.getFills();
			for (Persona fill: fills) {
				Parella<Persona, Persona> pares = fill.getPares();
				Persona pare = pares.getEsquerra();
				if (pare != null && !p.equals(pare)) {
					result.add(pare);
				}
				Persona mare = pares.getDreta();
				if (mare != null && !p.equals(mare)) {
					result.add(mare);
				}
			}
			
			return result;
		}
	}

	public Collection<Persona> getFillsEnComu(Persona pare, Persona mare) throws IllegalArgumentException {
		
		if (pare == null || mare == null) {
			if (pare == null && mare == null) {
				throw new IllegalArgumentException("null," + "null");
				
			}else if (pare == null && mare != null) {
				throw new IllegalArgumentException("null," + mare.getNom());
				
			}else {
				throw new IllegalArgumentException(pare.getNom() + ",null");
			}
			
		}else {
			Collection<Persona> fillsPare = pare.getFills();
			Collection<Persona> fillsMare = mare.getFills();
			
			Set<Persona> fills = new HashSet<>();
			for (Persona p: fillsPare) {
				if (fillsMare.contains(p)) {
					fills.add(p);
				}
			}
			
			return fills;
		}

	}

}
