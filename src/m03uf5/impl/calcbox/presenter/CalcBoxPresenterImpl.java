package m03uf5.impl.calcbox.presenter;

import m03uf5.impl.calcbox.model.CalcBoxModelImpl;
import m03uf5.prob.calcbox.CalcBoxContract;
import m03uf5.prob.calcbox.CalcBoxContract.CalcBoxView;

public class CalcBoxPresenterImpl implements CalcBoxContract.CalcBoxPresenter{

	private CalcBoxView view;

	public void setView(CalcBoxView v) {
		this.view = v;
		
	}

	public void calcula(String expressio) {
		CalcBoxModelImpl calcul = new CalcBoxModelImpl();
		try {
			view.mostra(String.valueOf(calcul.calcular(expressio)));
		}catch (Exception e) {
			view.mostra("Error caracters invalids");
			
		}
	}

}
