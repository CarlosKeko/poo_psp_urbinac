package m03uf5.impl.calcbox;

import javafx.application.Application;
import javafx.stage.Stage;
import m03uf5.impl.calcbox.presenter.CalcBoxPresenterImpl;
import m03uf5.impl.calcbox.view.CalcBoxViewImpl;
import m03uf5.prob.calcbox.CalcBoxContract.CalcBoxView;
import m03uf5.prob.calcbox.CalcBoxContract.CalcBoxPresenter;

public class CalcBoxApp extends Application {

	private CalcBoxView v;
	private CalcBoxPresenter p;
	
	public void start(Stage primaryStage) throws Exception {
		p = new CalcBoxPresenterImpl();
		v = new CalcBoxViewImpl(primaryStage);

		p.setView(v);
		v.setPresenter(p);
		
	}
	
	public static void main(String[] args) {
		launch(args);
	}

}
