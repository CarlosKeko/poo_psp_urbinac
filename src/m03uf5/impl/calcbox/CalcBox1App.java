package m03uf5.impl.calcbox;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import m03uf5.impl.calcbox.model.CalcBoxModelImpl;

public class CalcBox1App extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		CalcBoxModelImpl model = new CalcBoxModelImpl();
		
		primaryStage.setTitle("Calculadora");
		
        TextField textField = new TextField();
        textField.setText("");
        
        Button btn = new Button();
        btn.setText("Calcula");
        
        Label lbl = new Label();
        lbl.setText("");
        btn.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
            	try {
                	lbl.setText(String.valueOf(model.calcular(textField.getText().toString())));
                	
            	}catch (Exception e) {
            		e.printStackTrace();
            		System.out.println("Error carracter invalid");
            		
            	}

            }
        });
        
        VBox root = new VBox();
        root.setAlignment(Pos.CENTER);
        
        root.getChildren().addAll(textField, btn, lbl);
        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();
		
	}
	
	public static void main(String[] args) {
		launch(args);
	}

}
