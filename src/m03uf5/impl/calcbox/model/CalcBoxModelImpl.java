package m03uf5.impl.calcbox.model;

import m03uf5.prob.calcbox.ExprAvaluator;

public class CalcBoxModelImpl {
	
	public double calcular(String calcul) throws Exception{	
		ExprAvaluator calcular = new ExprAvaluator();
		return calcular.avalua(calcul);
		
	}
	
}
