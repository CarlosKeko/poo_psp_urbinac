package m03uf5.impl.calcbox.view;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import m03uf5.prob.calcbox.CalcBoxContract;
import m03uf5.prob.calcbox.CalcBoxContract.CalcBoxPresenter;

public class CalcBoxViewImpl implements CalcBoxContract.CalcBoxView {

	private CalcBoxPresenter presenter;
	private Label lbl;
	
	public CalcBoxViewImpl(Stage primaryStage) {
		setStage(primaryStage);
	}

	public void setPresenter(CalcBoxPresenter p) {
		this.presenter = p;
		
	}

	public void mostra(String resultat) {
		this.lbl.setText(resultat);
		
	}
	
	private void setStage(Stage primaryStage) {
		primaryStage.setTitle("Calculadora");
		
        TextField textField = new TextField();
        textField.setText("");
        textField.setPrefHeight(80);
        textField.setPrefWidth(80);
        
        Button btn = new Button();
        btn.setText("Calcula");
        btn.setPrefHeight(80);
        btn.setPrefWidth(80);
        
        lbl = new Label();
        lbl.setText("");
        lbl.setPrefHeight(80);
        
        btn.setOnAction(event -> presenter.calcula(textField.getText().toString()));
        
        VBox root = new VBox();
        root.setAlignment(Pos.CENTER);
        
        root.getChildren().addAll(textField, btn, lbl);
        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();
		
	}

}
