package m03uf5.impl.mascotes;

import m03uf5.prob.mascotes.Llar;

public class LlarImpl implements Llar{
	
	String nomLlar;
	
	public LlarImpl(String n) {
		this.nomLlar = n;
	}
	
	public LlarImpl(Llar l) {
		this.nomLlar = l.getNom();
	}
	
	public String getNom() {
		return nomLlar;
	}

}
