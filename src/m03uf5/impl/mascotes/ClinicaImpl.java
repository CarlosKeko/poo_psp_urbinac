package m03uf5.impl.mascotes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import m03uf5.prob.mascotes.Clinica;
import m03uf5.prob.mascotes.Llar;
import m03uf5.prob.mascotes.Mascota;
import m03uf5.prob.mascotes.Veterinari;

public class ClinicaImpl implements Clinica{

	public static Map<Long, MascotaImpl> mascotesRegistrades = new HashMap<>();
	private static Long nXip = 1L;
	public LlarImpl llar;
	private MascotaImpl mascota;
	private VeterinariImpl veterinari;
	
	public Llar creaLlar(String nom) {
		this.llar = new LlarImpl(nom);
		return llar;
	}

	public Veterinari creaVeterinari(String nom) {
		this.veterinari = new VeterinariImpl(nom);
		return this.veterinari;
	}

	public Mascota registraGos(String nom, int any, Llar llar) {
		this.mascota = new MascotaImpl(nom, any, nXip, llar);
		mascotesRegistrades.put(nXip, mascota);
		nXip++;
		return this.mascota;
	}

	public Mascota registraGat(String nom, int any, Llar llar) {
		this.mascota = new MascotaImpl(any, nom, nXip, llar);
		mascotesRegistrades.put(nXip, mascota);
		nXip++;
		return this.mascota;
	}

	public Mascota trobaMascota(Long xipId) {
		this.mascota = mascotesRegistrades.get(xipId);
		return this.mascota;
	}

	public List<Mascota> llistaMascotes(Llar l) {
		List<Mascota> mascotes = new ArrayList<Mascota>();
		
		for (Long num = 1L; num <= mascotesRegistrades.size(); num++) {
			if (this.trobaMascota(num).getXip().getLlar().getNom().equalsIgnoreCase(llar.getNom())) {
				mascotes.add(this.trobaMascota(num));
				
			}
			
		}
		return mascotes;
	}

}
