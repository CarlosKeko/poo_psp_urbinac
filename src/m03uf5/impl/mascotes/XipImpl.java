package m03uf5.impl.mascotes;
import m03uf5.prob.mascotes.Llar;
import m03uf5.prob.mascotes.Xip;

public class XipImpl implements Xip{

	private long nXip;
	private LlarImpl llar;
	
	//CONSTRUCTORS
	
	/**
	 * Constructor per defecte
	 */
	public XipImpl() {
		nXip = 0;
	}
	
	public XipImpl(long n, Llar l) {
		nXip = n;
		this.llar = new LlarImpl(l);
	}
	
	//GETERS
	
	public Long getId() {
		return this.nXip;
	}

	public Llar getLlar() {
		return this.llar;
	}

}
