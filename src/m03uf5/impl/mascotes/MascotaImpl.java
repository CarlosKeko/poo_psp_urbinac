package m03uf5.impl.mascotes;

import m03uf5.prob.mascotes.Especie;
import m03uf5.prob.mascotes.Llar;
import m03uf5.prob.mascotes.Mascota;
import m03uf5.prob.mascotes.Xip;

public class MascotaImpl implements Mascota{
	
	private String nom;
	private int any;
	private Especie especie;
	private XipImpl xip;
	
	
	//CONSTRUCTORES
	
	
	/**
	 * Constructor per defecte
	 */
	public MascotaImpl() {
		this.nom = "Sense nom";
		this.any = 1;
	}
	
	
	/**
	 * Constructor per a Gat
	 * @param a
	 * @param n
	 * @param x
	 * @param l
	 */
	public MascotaImpl(int a, String n, long x, Llar l) {
		this.nom = n;
		this.any = a;
		this.especie = Especie.GAT;
		this.xip = new XipImpl(x, l);
	}
	
	
	/**
	 * Constructor per a Gos
	 * @param n
	 * @param e
	 * @param a
	 */
	public MascotaImpl(String n, int a, long x, Llar l) {
		this.nom = n;
		this.any = a;
		this.especie = Especie.GOS;
		this.xip = new XipImpl(x, l);
	}

	
	//METODES
	
	/**
	 * Si retorna -1 es menor, si retorna 0 es igual i si es 1 �s major que el parametre.
	 */
	public int compareTo(Mascota arg0) {
		if (this.getAny() > arg0.getAny()) {
			return 1;
			
		}else if (this.getAny() < arg0.getAny()) {
			return -1;
			
		}else {
			return 0;
		}

	}

	public Xip getXip() {
		return this.xip;
	}

	public String getNom() {
		return this.nom;
	}

	public int getAny() {
		return this.any;
	}

	public Especie getEspecie() {
		return this.especie;
	}

	public String toString() {
		return "[" + this.getNom() + "(" +  this.getAny() + ") �s un " + this.getEspecie() + " amb chip " + this.getXip().getId() + " a " + this.getXip().getLlar().getNom() + "]";
	
	}
}
