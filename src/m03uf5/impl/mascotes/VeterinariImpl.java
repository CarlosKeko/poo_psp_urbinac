package m03uf5.impl.mascotes;

import java.util.LinkedList;
import java.util.Queue;

import m03uf5.prob.mascotes.Mascota;
import m03uf5.prob.mascotes.Veterinari;

public class VeterinariImpl implements Veterinari{
	
	private String nom;
	private Queue<Mascota> mascotes = new LinkedList<>();
	
	public VeterinariImpl() {
		this.nom = "default";
	}
	
	public VeterinariImpl(String n) {
		this.nom = n;
	}

	public String getNom() {
		return this.nom;
	}

	public int pendents() {
		return this.mascotes.size();
	}
 
	public void visita(Mascota mascota) {
		this.mascotes.add(mascota);
		
	}

	public Mascota atendre() {
		return this.mascotes.poll();

	}

}
