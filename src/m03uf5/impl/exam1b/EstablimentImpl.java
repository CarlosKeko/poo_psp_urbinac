package m03uf5.impl.exam1b;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import m03uf5.prob.exam1b.Comanda;
import m03uf5.prob.exam1b.Establiment;
import m03uf5.prob.exam1b.Proveidor;

public class EstablimentImpl<T> implements Establiment<T> {

	private HashMap<T, Proveidor<T>> proveidors = new HashMap<T, Proveidor<T>>();
	private HashMap<T, Integer> estoc = new HashMap<T, Integer>();
	
	public void setProveidor(Proveidor<T> p) {
		this.proveidors.put(p.getProducte(), p);
		this.estoc.put(p.getProducte(), 0);
		
	}

	public Proveidor<T> getProveidor(T t) {
		return this.proveidors.get(t);
	}

	public int getEstoc(T t) {
		return this.estoc.get(t);
	}

	public List<Comanda<T>> processar(List<Comanda<T>> comandes) {
		List<Comanda<T>> orders = new ArrayList<Comanda<T>>();
		int estocs;
		
		for (Comanda<T> com : comandes) {
			estocs = 0;
			if (!this.estoc.containsKey(com.getProducte())) {
				this.estoc.put(com.getProducte(), 0);
				
			}else if (this.proveidors.get(com.getProducte()) != null) {
				estocs = this.proveidors.get(com.getProducte()).subministrar(com.getQuantitat());
				
			}
			
			if (estocs == 0) {
				orders.add(com);
				
			}else {
				this.estoc.put(com.getProducte(), this.estoc.get(com.getProducte()) + estocs);
				
			}
		}
		return orders;
	}

}
