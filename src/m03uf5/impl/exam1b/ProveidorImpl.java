package m03uf5.impl.exam1b;

import m03uf5.prob.exam1b.Proveidor;

public class ProveidorImpl<T> implements Proveidor<T> {

	private T producte;
	private int estoc;
	
	public ProveidorImpl(T pro, int est) {
		this.producte = pro;
		this.estoc = est;
	}
	
	public T getProducte() {
		return this.producte;
	}

	public int getEstoc() {
		return this.estoc;
	}

	public void produir(int quantitat) {
		this.estoc = this.estoc + quantitat;
		
	}

	public int subministrar(int quantitat) {
		if (this.estoc >= quantitat) {
			this.estoc = this.estoc - quantitat;
			return quantitat;
			
		}else {
			return 0;
			
		}
	}

}
