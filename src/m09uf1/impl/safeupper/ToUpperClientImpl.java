package m09uf1.impl.safeupper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.logging.Logger;

import m09uf1.prob.safeupper.ToUpperClient;

public class ToUpperClientImpl implements ToUpperClient {
	
	static final Logger LOGGER = Logger.getLogger(ToUpperClientImpl.class.getName());
	
	static final Charset CHARSET = ToUpperServerImpl.CHARSET;

	private Socket socket;
	private String host;
	private int port;
	private PrintWriter out;
	private BufferedReader in;
	private CodecImpl codecImpl;
	
	public ToUpperClientImpl(String host, int port, byte[] secretKey) throws IOException {		
		this.host = host;
		this.port = port;
		this.codecImpl = new CodecImpl(secretKey);
	}
	
	public void connect() throws IOException {
		
		socket = new Socket();
		socket.connect(new InetSocketAddress(host, port), 1500); // connect timeout
		socket.setSoTimeout(1000); // response timeout
		
		out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), CHARSET), true);
		in = new BufferedReader(new InputStreamReader(socket.getInputStream(), CHARSET));
	}
	
	@Override
	public void disconnect() throws IOException {
		try {
			socket.close();
		} finally {
			socket = null;
		}
	}

	@Override
	public synchronized String toUpper(String input) throws IOException {
		
		if (socket == null) {
			throw new IllegalStateException("socket is not created");
		}
		
		out.println(codecImpl.encode(input));
		String received = in.readLine();
		if (received == null) {
			socket = null;
			throw new IOException("socket is not connected");
		}
		
		LOGGER.info("received encode is " + received);
		
		received = codecImpl.decode(received);
		
		LOGGER.info("received decode is " + received);		
		return received;
	}
}
