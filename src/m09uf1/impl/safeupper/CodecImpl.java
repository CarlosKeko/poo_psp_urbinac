package m09uf1.impl.safeupper;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.Base64;
import java.util.logging.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import lib.CoDec;
import lib.CoDecException;

public class CodecImpl implements CoDec<String, String>{
	
	private Cipher cipherEncode;
	private Cipher cipherDecode;
	
	static final Charset CHARSET = StandardCharsets.UTF_8;
	static final Logger LOGGER = Logger.getLogger(CodecImpl.class.getName());
	
	public CodecImpl(byte[] secretKey) throws CoDecException{
		try {
			SecretKey myKey = new SecretKeySpec(secretKey, "AES");
			cipherEncode = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipherDecode = Cipher.getInstance("AES/ECB/PKCS5Padding");
			
			cipherEncode.init(Cipher.ENCRYPT_MODE, myKey);
			cipherDecode.init(Cipher.DECRYPT_MODE, myKey);
			
		} catch (GeneralSecurityException e) {
			throw new CoDecException("Error " + e.getMessage());
			
		}
	}
	
	public String encode(String i) throws CoDecException {
		try {
			byte[] bytesCifrados = cipherEncode.doFinal(i.getBytes());
			return Base64.getEncoder().encodeToString(bytesCifrados);
			
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			throw new CoDecException("Error " + e.getMessage());
			
		}			
	}

	public String decode(String o) throws CoDecException {
		try {
			byte[] bytesDescodificados = Base64.getDecoder().decode(o);
			return new String(cipherDecode.doFinal(bytesDescodificados), CHARSET);
			
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			throw new CoDecException("Error " + e.getMessage());
			
		}
	}
}
