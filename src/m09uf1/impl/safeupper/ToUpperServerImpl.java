package m09uf1.impl.safeupper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import lib.Startable;

public class ToUpperServerImpl implements Startable {

	static final Logger LOGGER = Logger.getLogger(ToUpperServerImpl.class.getName());
	
	static final int PORT = 5508;
	static final int NTHREADS = 10;
	
	static final Charset UTF8 = StandardCharsets.UTF_8;
	static final Charset ISO8859_1 = StandardCharsets.ISO_8859_1;
	static final Charset CHARSET = UTF8; // it could be UTF8, ISO8859_1...
	
	static final int TIMEOUT_MILLIS = 1500;
	
	private String host;
	private int port;
	private volatile ServerSocket serverSocket;
	private CodecImpl codecImpl;
	
	public ToUpperServerImpl(String host, int port, byte[] secretKey) throws IOException {
		this.host = host;
		this.port = port;
		this.codecImpl = new CodecImpl(secretKey);
	}
	
	public void start() {
		new Thread(() -> server(), "toupper").start();
	}

	public void stop() {
		try {
			serverSocket.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private void server() {
		
		ExecutorService executor = Executors.newCachedThreadPool();
		
		try {				
			this.serverSocket = new ServerSocket(port, 10, InetAddress.getByName(host));
			serverSocket.setSoTimeout(TIMEOUT_MILLIS);
			
			while (true) {				
				try {
					Socket clientSocket = serverSocket.accept();					
					
					SocketAddress remote = clientSocket.getRemoteSocketAddress();					
					LOGGER.info("accept from " + remote);
					
					clientSocket.setSoTimeout(TIMEOUT_MILLIS);					
					executor.submit(() -> handle(clientSocket, codecImpl));
					
				} catch (SocketTimeoutException e) {
					// timeout in the accept: ignore
					LOGGER.fine("timeout accepting!");
				}
			}
			
		} catch (IOException e) {
			if (serverSocket.isClosed()) {
				LOGGER.info("server was closed");
				return;
			}
			
			LOGGER.log(Level.SEVERE, "serving", e);
			
		} finally {
			executor.shutdown();
		}
	}
	
	static void handle(Socket clientSocket, CodecImpl codecImpl) {
		
		try (PrintWriter out = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream(), CHARSET), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), CHARSET))) {
			
			while (true) {
				try {
					String input = in.readLine();
					if (input == null) {
						LOGGER.warning("client disconnected!");
						return;
					}
					LOGGER.info("request encode is " + input);
					input = codecImpl.decode(input);
					
					LOGGER.info("request decode is " + input);
					out.println(codecImpl.encode(input.toUpperCase()));
					
				} catch (SocketTimeoutException e) {
					// read timeout: do nothing
					LOGGER.fine("timeout reading!");
				}
			}
									
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "handling", e);
			
		} finally {
			try {
				clientSocket.close();
			} catch (IOException e) {
				LOGGER.warning("closing: " + e.getMessage());
			}
		}
	}

	public boolean isStarted() {
		return !serverSocket.isClosed();
	}
}
