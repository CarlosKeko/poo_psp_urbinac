package m09uf1.impl.passmgr;

import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.HashMap;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.json.JSONException;
import org.json.JSONObject;

import lib.db.DuplicateException;
import lib.db.NotFoundException;
import m09uf1.prob.passmgr.PassManager;

public class PassManagerImpl implements PassManager {

	private JSONObject jsonObject;

	public PassManagerImpl(JSONObject jsonObject) {
		this.jsonObject = jsonObject;

	}

	public void addEntry(String user, char[] pass) throws DuplicateException {
		try {
			if (!jsonObject.has(user)) {
				HashMap<String, String> saltYPass = generateSecretHash(String.valueOf(pass));
				jsonObject.put(user,
						new JSONObject().put("pass", saltYPass.get("pass")).put("salt", saltYPass.get("salt")));
				
			} else {
				throw new DuplicateException("Error el user ya existe");
			}

		} catch (GeneralSecurityException e) {
			throw new RuntimeException("Error ", e);
		}

	}

	public void deleteEntry(String user) throws NotFoundException {
		if (jsonObject.has(user)) {
			jsonObject.remove(user);

		} else {
			throw new NotFoundException("Error el usuario no existe");

		}
	}

	public boolean checkPassword(String user, char[] pass) throws NotFoundException {
		try {
			String saltUser = jsonObject.getJSONObject(user).getString("salt");
			String passUser = jsonObject.getJSONObject(user).getString("pass");
			String passCodificada = codificarPass(String.valueOf(pass), Base64.getDecoder().decode(saltUser));

			if (passUser.equals(passCodificada)) {
				return true;

			} else {
				return false;
			}

		} catch (JSONException e) {
			throw new NotFoundException("Error", e);

		} catch (GeneralSecurityException e) {
			throw new RuntimeException("Error ", e);
		}
	}

	public boolean setPassword(String user, char[] oldpass, char[] newpass) throws NotFoundException {
		try {
			String saltUser = jsonObject.getJSONObject(user).getString("salt");
			String passUser = jsonObject.getJSONObject(user).getString("pass");
			String passCodificada = codificarPass(String.valueOf(oldpass), Base64.getDecoder().decode(saltUser));

			if (passUser.equals(passCodificada)) {
				HashMap<String, String> passYSalt = generateSecretHash(String.valueOf(newpass));
				jsonObject.getJSONObject(user).put("salt", passYSalt.get("salt"));
				jsonObject.getJSONObject(user).put("pass", passYSalt.get("pass"));
				return true;

			} else {
				return false;
			}

		} catch (JSONException e) {
			throw new NotFoundException("Error", e);

		} catch (GeneralSecurityException e) {
			throw new RuntimeException("Error ", e);
		}
	}

	public HashMap<String, String> generateSecretHash(String password) throws GeneralSecurityException {
		SecureRandom random = new SecureRandom();
		byte[] salt = random.generateSeed(16);
		SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
		KeySpec ks = new PBEKeySpec(password.toCharArray(), salt, 16384, 256);
		SecretKey s = f.generateSecret(ks);
		HashMap<String, String> saltYPass = new HashMap<String, String>();
		saltYPass.put("salt", Base64.getEncoder().encodeToString(salt));
		saltYPass.put("pass", Base64.getEncoder().encodeToString(s.getEncoded()));
		return saltYPass;
	}

	public String codificarPass(String password, byte[] salt) throws GeneralSecurityException {
		SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
		KeySpec ks = new PBEKeySpec(password.toCharArray(), salt, 16384, 256);
		SecretKey s = f.generateSecret(ks);
		return Base64.getEncoder().encodeToString(s.getEncoded());
	}

	public String toString() {
		return jsonObject.toString();
	}

}
