package m09uf1.impl.safeupper2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

import m09uf1.prob.safeupper2.ToUpperClient;

public class ToUpperClientImpl implements ToUpperClient {
	
	static final Logger LOGGER = Logger.getLogger(ToUpperClientImpl.class.getName());
	
	static final Charset UTF8 = StandardCharsets.UTF_8;
	static final Charset ISO8859_1 = StandardCharsets.ISO_8859_1;
	static final Charset CHARSET = UTF8; // it could be UTF8, ISO8859_1...

	private Socket socket;
	private String host;
	private int port;
	private PrintWriter out;
	private BufferedReader in;
	private CoDecClient codec;
	
	public ToUpperClientImpl(String host, int port) {		
		this.host = host;
		this.port = port;
		this.codec = new CoDecClient();
	}
	
	public void connect() throws IOException {
		createSocket();
		String inputXif = codec.getPublic();		
		out.println(inputXif);
		
		String received = in.readLine();
		if (received == null) {
			socket = null;
			throw new IOException("socket is not connected");
		}
		codec.readSecret(received);
		
	}
	
	public void disconnect() throws IOException {
		try {
			socket.close();
		} finally {
			socket = null;
		}
	}

	public synchronized String toUpper(String input) throws IOException {
		
		if (socket == null) {
			throw new IllegalStateException("socket is not created");
		}
		
		String inputXif = codec.encode(input);		
		out.println(inputXif);
		String received = in.readLine();
		if (received == null) {
			socket = null;
			throw new IOException("socket is not connected");
		}
		
		String receivedDes = codec.decode(received);		
		LOGGER.info("received " + receivedDes);		
		return receivedDes;
	}

	private void createSocket() throws IOException {
		
		socket = new Socket();
		socket.connect(new InetSocketAddress(host, port), 1500); // connect timeout
		socket.setSoTimeout(1000); // response timeout
		
		out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), CHARSET), true);
		in = new BufferedReader(new InputStreamReader(socket.getInputStream(), CHARSET));
	}
}
