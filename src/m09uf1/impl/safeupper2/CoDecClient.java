package m09uf1.impl.safeupper2;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.logging.Logger;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import lib.CoDec;
import lib.CoDecException;

public class CoDecClient implements CoDec<String, String> {

	static final Charset CHARSET = StandardCharsets.UTF_8;
	static final Logger LOGGER = Logger.getLogger(CoDecClient.class.getName());
	
	private PrivateKey privateKeyRSA;
	private SecretKeySpec privateKeyServer;
	private Cipher ecipher, dcipher;
	
	public String getPublic() throws CoDecException {
		try {
			KeyPair kp = this.generateKeyPair("RSA", 1024);
			byte[] publicKey = kp.getPublic().getEncoded();
			privateKeyRSA = kp.getPrivate();
			LOGGER.info("private bytes " + privateKeyRSA.getAlgorithm());
			
			return Base64.getEncoder().encodeToString(publicKey);
			
		} catch (GeneralSecurityException e) {
			throw new CoDecException("Error ", e);
			
		} 
	}
	
	public void readSecret(String clave) throws CoDecException {
		try {
			Cipher cipherDecodeKey = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipherDecodeKey.init(Cipher.DECRYPT_MODE, privateKeyRSA);
			
			byte[] decodeKey = Base64.getDecoder().decode(clave);
			byte[] claveDescifrada = cipherDecodeKey.doFinal(decodeKey);
			
			privateKeyServer = new SecretKeySpec(claveDescifrada, "AES");
			createChipers();
			
		} catch (GeneralSecurityException e) {
			throw new CoDecException("Error ", e);
			
		} 	
	}
	
	private void createChipers() throws CoDecException {
		try {
			if (privateKeyServer == null) {
				throw new IllegalStateException("private key is not created");
			}
			
			this.ecipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			this.ecipher.init(Cipher.ENCRYPT_MODE, privateKeyServer);
			this.dcipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			this.dcipher.init(Cipher.DECRYPT_MODE, privateKeyServer);
			
		} catch (GeneralSecurityException e) {
			throw new CoDecException("creating cipher", e);
		}
	}
	
	public String encode(String input) throws CoDecException {
		byte[] inputArr = input.getBytes(CHARSET);
		try {
			byte[] outputArr = ecipher.doFinal(inputArr);
			String output = Base64.getEncoder().encodeToString(outputArr);
			LOGGER.fine("xifrant " + input + " > " + output);
			return output;
			
		} catch (GeneralSecurityException e) {
			throw new CoDecException("ciphering", e);
		}	
	}

	public String decode(String input) throws CoDecException {
		byte[] inputArr;
		try {
			inputArr = Base64.getDecoder().decode(input);
			
		} catch (IllegalArgumentException e) {
			throw new CoDecException("decoding input", e);
		}
		
		try {
			byte[] outputArr = dcipher.doFinal(inputArr);
			String output = new String(outputArr, CHARSET);
			LOGGER.fine("desxifrant " + input + " > " + output);
			return output;
			
		} catch (GeneralSecurityException e) {
			throw new CoDecException("deciphering", e);
		}
	}
    
    public KeyPair generateKeyPair(String algorithm, int size) throws NoSuchAlgorithmException {     
        KeyPairGenerator kpg = KeyPairGenerator.getInstance(algorithm);
        kpg.initialize(size);
        KeyPair kp = kpg.generateKeyPair();
        return kp;      
    } 
    
    public PrivateKey importPrivateKey(KeyFactory kf, byte[] encodedKey) throws InvalidKeySpecException {
        PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(encodedKey);
        PrivateKey privateKey = kf.generatePrivate(keySpecPKCS8);
        return privateKey; 
    }

}
