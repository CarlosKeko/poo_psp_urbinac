package m09uf1.impl.safeupper2;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.logging.Logger;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

import lib.CoDec;
import lib.CoDecException;

public class CoDecServer implements CoDec<String, String>{

	static final Charset CHARSET = StandardCharsets.UTF_8;
	static final Logger LOGGER = Logger.getLogger(CoDecServer.class.getName());
	
	private SecretKeySpec secretKey;
	private Cipher ecipher, dcipher;
	
	public String getSecret(String clavePublica) throws CoDecException {	
		try {
			byte[] publicKeyBytes = Base64.getDecoder().decode(clavePublica);
			
	        KeyFactory kf = KeyFactory.getInstance("RSA");
	        PublicKey publicKey = importPublicKey(kf, publicKeyBytes);
			
			KeyGenerator keyGen = KeyGenerator.getInstance("AES");
	        keyGen.init(128);
	        secretKey = (SecretKeySpec) keyGen.generateKey();
	        createChipers();
			
			Cipher cipherEncodeKey = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipherEncodeKey.init(Cipher.ENCRYPT_MODE, publicKey);
	        
			byte[] claveEncriptada = cipherEncodeKey.doFinal(secretKey.getEncoded());
			
	        return Base64.getEncoder().encodeToString(claveEncriptada);
	        
		} catch (GeneralSecurityException e) {
			throw new CoDecException("Error ", e);
			
		}
		
	}
	
	private void createChipers() throws CoDecException {
		try {
			if (secretKey == null) {
				throw new IllegalStateException("private key is not created");
			}
			
			this.ecipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			this.ecipher.init(Cipher.ENCRYPT_MODE, secretKey);
			this.dcipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			this.dcipher.init(Cipher.DECRYPT_MODE, secretKey);
			
		} catch (GeneralSecurityException e) {
			throw new CoDecException("creating cipher", e);
		}
		
	}
	
	public String encode(String input) throws CoDecException {
		byte[] inputArr = input.getBytes(CHARSET);
		try {
			byte[] outputArr = ecipher.doFinal(inputArr);
			String output = Base64.getEncoder().encodeToString(outputArr);
			LOGGER.fine("xifrant " + input + " > " + output);
			return output;
			
		} catch (GeneralSecurityException e) {
			throw new CoDecException("ciphering", e);
		}
		
	}

	public String decode(String input) throws CoDecException {
		byte[] inputArr;
		try {
			inputArr = Base64.getDecoder().decode(input);
			
		} catch (IllegalArgumentException e) {
			throw new CoDecException("decoding input", e);
		}
		
		try {
			byte[] outputArr = dcipher.doFinal(inputArr);
			String output = new String(outputArr, CHARSET);
			LOGGER.fine("desxifrant " + input + " > " + output);
			return output;
			
		} catch (GeneralSecurityException e) {
			throw new CoDecException("deciphering", e);
		}
		
	}
	
    public PublicKey importPublicKey(KeyFactory kf, byte[] encodedKey) throws InvalidKeySpecException {
        X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(encodedKey);
        PublicKey publicKey = kf.generatePublic(keySpecX509);
        return publicKey;
        
    }   

}
