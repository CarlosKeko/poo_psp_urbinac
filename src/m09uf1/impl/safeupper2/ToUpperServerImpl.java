package m09uf1.impl.safeupper2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import lib.Startable;

public class ToUpperServerImpl implements Startable {

	static final Logger LOGGER = Logger.getLogger(ToUpperServerImpl.class.getName());
	
	static final int NTHREADS = 10;
	
	static final Charset UTF8 = StandardCharsets.UTF_8;
	static final Charset ISO8859_1 = StandardCharsets.ISO_8859_1;
	static final Charset CHARSET = UTF8; // it could be UTF8, ISO8859_1...
	
	static final int TIMEOUT_MILLIS = 1500;
	
	private String host;
	private int port;
	private Thread thread;
	private volatile ServerSocket serverSocket;
	
	public ToUpperServerImpl(String host, int port) {
		this.host = host;
		this.port = port;
	}
	
	public void start() {
		thread = new Thread(() -> server(), "toupper");
		thread.start();
	}

	public void stop() {
		try {
			serverSocket.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public boolean isStarted() {
		return thread != null && thread.isAlive();
	}
	
	private void server() {
		
		ExecutorService executor = Executors.newCachedThreadPool();
		
		LOGGER.info("starting server at " + host + ":" + port);
		
		try {				
			this.serverSocket = new ServerSocket(port, 10, InetAddress.getByName(host));
			serverSocket.setSoTimeout(TIMEOUT_MILLIS);
			
			while (true) {				
				try {
					Socket clientSocket = serverSocket.accept();					
					
					SocketAddress remote = clientSocket.getRemoteSocketAddress();					
					LOGGER.info("accept from " + remote);
					
					clientSocket.setSoTimeout(TIMEOUT_MILLIS);					
					executor.submit(() -> handle(clientSocket));
					
				} catch (SocketTimeoutException e) {
					// timeout in the accept: ignore
					LOGGER.fine("timeout accepting!");
				}
			}
			
		} catch (IOException e) {
			if (serverSocket.isClosed()) {
				LOGGER.info("server was closed");
				return;
			}
			
			LOGGER.log(Level.SEVERE, "serving", e);
			
		} finally {
			executor.shutdown();
		}
	}
	
	static void handle(Socket clientSocket) {
		
		try (PrintWriter out = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream(), CHARSET), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), CHARSET))) {
			
			CoDecServer codec = new CoDecServer();
			
			String inputPublicKey = in.readLine();
			if (inputPublicKey == null) {
				LOGGER.warning("client disconnected!");
				return;
			}
			
			String sendPrivateKey = codec.getSecret(inputPublicKey);
			LOGGER.info("request key private base64 is " + sendPrivateKey);
			out.println(sendPrivateKey);
			
			while (true) {
				try {
					String input = in.readLine();
					if (input == null) {
						LOGGER.warning("client disconnected!");
						return;
					}
					
					String inputDes = codec.decode(input);					
					LOGGER.info("request is " + inputDes);
					
					String sent = codec.encode(inputDes.toUpperCase());					
					out.println(sent);
					
				} catch (SocketTimeoutException e) {
					// read timeout: do nothing
					LOGGER.fine("timeout reading!");
				}
			}
									
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "handling", e);
			
		} finally {
			try {
				clientSocket.close();
			} catch (IOException e) {
				LOGGER.warning("closing: " + e.getMessage());
			}
		}
	}

}
