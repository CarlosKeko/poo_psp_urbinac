package m09uf1.impl.speerb;

import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.function.Consumer;
import java.util.logging.Logger;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

import m09uf1.prob.speerb.SafePeer;

public class SafePeerImpl implements SafePeer{

	private SecretKeySpec secretKey;
	private PrivateKey privateKeyRSA;
	private PublicKey pk;
	private Cipher ecipher, dcipher;
	private SafePeer peer;
	private Consumer<String> consumer;
	
	static final Logger LOGGER = Logger.getLogger(SafePeerImpl.class.getName());
	
	public SafePeerImpl(Consumer<String> consumer) {
		this.consumer = consumer;
		
	}
	
	public PublicKey getPublicKey() {
		try {
			KeyPair kp = this.generateKeyPair("RSA", 1024);
			pk = kp.getPublic();
			privateKeyRSA = kp.getPrivate();
			return pk;
			
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("Error ", e);
		}
	}

	public void connectTo(SafePeer to) {
		try {
			peer = to;
			pk = to.getPublicKey();
			KeyGenerator keyGen = KeyGenerator.getInstance("AES");
			keyGen.init(256);
			secretKey = (SecretKeySpec) keyGen.generateKey();
			
			Cipher cipherEncodeKey = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipherEncodeKey.init(Cipher.ENCRYPT_MODE, pk);
			
			byte[] claveEncriptada = cipherEncodeKey.doFinal(secretKey.getEncoded());
			createChipers();
			
//			LOGGER.info("Conectando a nuevo safe peer");
			
			to.connectFrom(this, claveEncriptada);
			
		} catch (GeneralSecurityException e) {
			throw new RuntimeException("Error ", e);
			
		}  
		
	}

	public void connectFrom(SafePeer from, byte[] cipheredSecret) {
		try {
			peer = from;
			Cipher cipherDecodeKey = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipherDecodeKey.init(Cipher.DECRYPT_MODE, privateKeyRSA);
			
			byte[] claveDescifrada = cipherDecodeKey.doFinal(cipheredSecret);
			
			secretKey = new SecretKeySpec(claveDescifrada, "AES");
			createChipers();
			
//			LOGGER.info("Conexion completada");
			
		} catch (GeneralSecurityException e) {
			throw new RuntimeException("Error ", e);
			
		} 
	}

	public void send(String message) {
		try {
			if (peer == null) {
				throw new RuntimeException("Error peer no valido");
			}
 			
			byte[] inputArr = message.getBytes(StandardCharsets.UTF_8);
			byte[] outputArr = ecipher.doFinal(inputArr);
//			LOGGER.info("xifrant " + message + " > " + outputArr);
			peer.receive(outputArr);
			
		} catch (GeneralSecurityException e) {
			throw new RuntimeException("ciphering", e);
		}
		
	}

	public void receive(byte[] cipheredBytes) {
		try {
			if (peer == null) {
				throw new RuntimeException("Error peer no valido");
			}
			
			byte[] outputArr = dcipher.doFinal(cipheredBytes);
			String output = new String(outputArr, StandardCharsets.UTF_8);
//			LOGGER.info("desxifrant " + cipheredBytes + " > " + output);
			consumer.accept(output);
			
		} catch (GeneralSecurityException e) {
			throw new RuntimeException("deciphering", e);
		}
		
	}
	
    public KeyPair generateKeyPair(String algorithm, int size) throws NoSuchAlgorithmException {     
        KeyPairGenerator kpg = KeyPairGenerator.getInstance(algorithm);
        kpg.initialize(size);
        KeyPair kp = kpg.generateKeyPair();
        return kp;      
    } 
    
	private void createChipers() {
		try {
			if (secretKey == null) {
				throw new RuntimeException("private key is not created");
			}
			
			this.ecipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			this.ecipher.init(Cipher.ENCRYPT_MODE, secretKey);
			this.dcipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			this.dcipher.init(Cipher.DECRYPT_MODE, secretKey);
			
		} catch (GeneralSecurityException e) {
			throw new RuntimeException("creating cipher", e);
		}
	}

}
