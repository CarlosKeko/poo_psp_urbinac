package m09uf1.impl.safestore;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Base64;

import org.json.JSONObject;

import lib.TextFileStorage;
import m09uf1.prob.safestore.SafeStore;
import m09uf1.prob.safestore.WrongKeyException;
import m09uf1.impl.safestore.GenerateCoDec;

public class SafeStoreImpl implements SafeStore {

	private TextFileStorage fileStorage;
	private String nomArxiu;
	private byte[] kek;
	private JSONObject jsonObject = new JSONObject();

	public SafeStoreImpl(String nomArxiu) {
		this.fileStorage = new TextFileStorage();
		this.nomArxiu = nomArxiu;
		this.jsonObject = new JSONObject();
	}

	public File getPath() {
		String path = fileStorage.getReadAbsolutePath(nomArxiu);
		return new File(path);
	}

	public void init(char[] password) throws IOException {
		try {
			if (getPath().exists() || !isClosed()) {
				throw new IOException("El archivo ya existe");
			}

			String salt = GenerateCoDec.generateRandomSecure();
			kek = GenerateCoDec.generateKek(String.valueOf(password), Base64.getDecoder().decode(salt));
			String kiv = GenerateCoDec.generateRandomSecure();
			String div = GenerateCoDec.generateRandomSecure();
			String dek = GenerateCoDec.generarXifrarDEK(kiv, kek);

			jsonObject = new JSONObject()
					.put("SALT", salt)
					.put("KIV", kiv)
					.put("DEK", dek)
					.put("DIV", div);

			fileStorage.writeToFile(nomArxiu, jsonObject.toString());

		} catch (IOException e) {
			throw new RuntimeException("Error ", e);
		}
	}

	public void open(char[] password) throws IOException {
		if (getPath().exists()) {
			try {
				jsonObject = new JSONObject(fileStorage.readFromFile(nomArxiu));
				kek = GenerateCoDec.generateKek(String.valueOf(password),
						Base64.getDecoder().decode(jsonObject.getString("SALT")));

			} catch (IOException e) {
				throw new IOException("Error", e);
			}

		} else {
			throw new IOException("El archivo no existe");

		}
	}

	public void close() {
		jsonObject = new JSONObject();
		kek = null;

	}

	public boolean isClosed() {
		return jsonObject.isEmpty();
		
	}

	public String get(String key) throws WrongKeyException {	
		try {
			if (isClosed()) {
				throw new WrongKeyException("Error storage cerrado");
			}
			
			if (jsonObject.isNull(key)) {
				return null;
			}
			
			byte[] dekDescifrada = GenerateCoDec.descifrarDEK(jsonObject.getString("KIV"), jsonObject.getString("DEK"), kek);
			String valorCifrado = GenerateCoDec.descifrarValor(dekDescifrada, jsonObject.getString("DIV"), jsonObject.getString(key));
			
			return valorCifrado;
			
		}catch (WrongKeyException e) {
			throw new WrongKeyException("Error", e);
			
		}
	}

	public void set(String key, String value) throws WrongKeyException {
		try {
			if (isClosed()) {
				throw new WrongKeyException("Error storage cerrado");
			}
			
			if (value == null) {
				jsonObject.put(key, value);
				fileStorage.writeToFile(nomArxiu, jsonObject.toString());
				
			}else {
				byte[] dekDescifrada = GenerateCoDec.descifrarDEK(jsonObject.getString("KIV"), jsonObject.getString("DEK"), kek);
				String valorCifrado = GenerateCoDec.cifrarValor(dekDescifrada, jsonObject.getString("DIV"), value);
				jsonObject.put(key, valorCifrado);
				fileStorage.writeToFile(nomArxiu, jsonObject.toString());
				
			}
			
		} catch (IOException e) {
			throw new WrongKeyException("Error ", e);
			
		}
	}

	public void setPassword(char[] usepass) throws WrongKeyException {
		if (isClosed()) {
			throw new WrongKeyException("Error storage cerrado");
		}
		
		try {
			byte[] dekDescifrada = GenerateCoDec.descifrarDEK(jsonObject.getString("KIV"), jsonObject.getString("DEK"), kek);
			kek = GenerateCoDec.generateKek(String.valueOf(usepass), Base64.getDecoder().decode(jsonObject.getString("SALT")));
			byte[] newDek = GenerateCoDec.cifrarDEK(jsonObject.getString("KIV"), dekDescifrada, kek);	
			
			jsonObject.put("DEK", Base64.getEncoder().encodeToString(newDek));
			fileStorage.writeToFile(nomArxiu, jsonObject.toString());
			
		} catch (GeneralSecurityException | IOException e) {
			throw new WrongKeyException("Error ", e);
			
		}
	}
}
