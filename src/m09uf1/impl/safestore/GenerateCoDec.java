package m09uf1.impl.safestore;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import m09uf1.prob.safestore.WrongKeyException;

public class GenerateCoDec {

	public static byte[] generateKek(String password, byte[] salt) throws IOException {
		try {
			SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
			KeySpec ks = new PBEKeySpec(password.toCharArray(), salt, 16384, 256);
			SecretKey s = f.generateSecret(ks);
			return s.getEncoded();
			
		} catch (GeneralSecurityException e) {
			throw new IOException("Error", e);
		}

	}
	
	public static String generateRandomSecure() {
		SecureRandom random = new SecureRandom();
		return Base64.getEncoder().encodeToString(random.generateSeed(16)); 
	}
	
	public static String generarXifrarDEK(String kiv, byte[] kek) throws IOException{			
		try {
			KeyGenerator keyGen = KeyGenerator.getInstance("AES");
		    keyGen.init(256);
		    SecretKey myKeyDEK = keyGen.generateKey();
			SecretKey keyKEK = new SecretKeySpec(kek, "AES");
		    
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			IvParameterSpec paramSpec = new IvParameterSpec(Base64.getDecoder().decode(kiv));
			
			cipher.init(Cipher.ENCRYPT_MODE, keyKEK, paramSpec);
			byte[] encrypted = cipher.doFinal(myKeyDEK.getEncoded());
			
			return Base64.getEncoder().encodeToString(encrypted);
			
		} catch (GeneralSecurityException e) {
			throw new IOException("Error", e);
		}
	}
	
	public static byte[] descifrarDEK(String kiv, String dek, byte[] kek) throws WrongKeyException{
		try {
			SecretKey keyKEK = new SecretKeySpec(kek, "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			IvParameterSpec paramSpec = new IvParameterSpec(Base64.getDecoder().decode(kiv));
			cipher.init(Cipher.DECRYPT_MODE, keyKEK, paramSpec);
			byte[] decryptedKey = cipher.doFinal(Base64.getDecoder().decode(dek));
			return decryptedKey;
		
		} catch (GeneralSecurityException e) {
			throw new WrongKeyException("Error", e);
			
		}	
	}
	
	public static byte[] cifrarDEK(String kiv, byte[] dek, byte[] kek) {
		try {
			SecretKey keyKEK = new SecretKeySpec(kek, "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			IvParameterSpec paramSpec = new IvParameterSpec(Base64.getDecoder().decode(kiv));
			cipher.init(Cipher.ENCRYPT_MODE, keyKEK, paramSpec);
			byte[] encryptedKey = cipher.doFinal(dek);
			return encryptedKey;
		
		} catch (GeneralSecurityException e) {
			throw new RuntimeException("Error", e);
			
		}	
		
	}
	
	public static String cifrarValor(byte[] dek, String div, String valor) throws WrongKeyException {
		try {
			SecretKey keyDEK = new SecretKeySpec(dek, "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			IvParameterSpec paramSpec = new IvParameterSpec(Base64.getDecoder().decode(div));
			cipher.init(Cipher.ENCRYPT_MODE, keyDEK, paramSpec);
			byte[] inputArr = valor.getBytes(StandardCharsets.UTF_8);
			byte[] encryptedValor = cipher.doFinal(inputArr);
			
			return Base64.getEncoder().encodeToString(encryptedValor);
			
		} catch (GeneralSecurityException e) {
			throw new WrongKeyException("Error", e);
			
		}
	}
	
	public static String descifrarValor(byte[] dek, String div, String valor) throws WrongKeyException {
		try {
			SecretKey keyDEK = new SecretKeySpec(dek, "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			IvParameterSpec paramSpec = new IvParameterSpec(Base64.getDecoder().decode(div));
			cipher.init(Cipher.DECRYPT_MODE, keyDEK, paramSpec);
			byte[] inputArr = Base64.getDecoder().decode(valor);
			byte[] outputArr = cipher.doFinal(inputArr);
			
			return new String(outputArr, StandardCharsets.UTF_8);
			
		} catch (GeneralSecurityException e) {
			throw new WrongKeyException("Error", e);
			
		}
		
	}
}
