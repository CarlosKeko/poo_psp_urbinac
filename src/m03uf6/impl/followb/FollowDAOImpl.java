package m03uf6.impl.followb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import lib.db.ConnectionFactory;
import lib.db.DuplicateException;
import lib.db.NotFoundException;
import m03uf6.prob.followb.FollowDAO;
import m03uf6.prob.followb.FollowDB;

public class FollowDAOImpl implements FollowDAO {

	private ConnectionFactory cf;

	public FollowDAOImpl(ConnectionFactory conn) {
		this.cf = conn;
	}

	@Override
	public void init() {
		FollowDB.init(cf);

	}

	@Override
	public boolean existeix(String usuari) {
		try (Connection conn = cf.getConnection()) {
			try (PreparedStatement stmt = conn.prepareStatement("SELECT nom FROM usuaris WHERE nom = ?")) {
				stmt.setString(1, usuari);

				ResultSet rs = stmt.executeQuery();

				if (rs.next()) {
					return true;

				} else {
					return false;
				}

			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return false;
	}

	@Override
	public void crear(String usuari) throws DuplicateException {
		try (Connection conn = cf.getConnection()) {
			try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO usuaris (nom) VALUES (?)")) {
				stmt.setString(1, usuari);
				stmt.executeUpdate();

			}

		} catch (SQLException e) {
			if (e.getErrorCode() == 1062) {
				throw new DuplicateException("ID duplicada");

			} else {
				e.printStackTrace();

			}
		}

	}

	@Override
	public void esborrar(String usuari) throws NotFoundException {
		try (Connection conn = cf.getConnection()) {
			conn.setAutoCommit(false);

			if (this.existeix(usuari)) {
				Set<String> seguits = this.getSeguits(usuari);
				Set<String> seguidors = this.getSeguidors(usuari);

				if (seguits.size() > 0) {
					for (String seguit : seguits) {
						this.deixarDeSeguir(usuari, seguit, conn);
					}
				}

				if (seguidors.size() > 0) {
					for (String seguidor : seguidors) {
						this.deixarDeSeguir(seguidor, usuari, conn);
					}
				}

				try (PreparedStatement stmt = conn.prepareStatement("DELETE FROM usuaris WHERE nom = ?")) {
					stmt.setString(1, usuari);
					stmt.executeUpdate();

				} catch (SQLException e) {
					conn.rollback();
				}

			} else {
				throw new NotFoundException("No se ha encontrado el registro");
			}

			conn.commit();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void seguir(String seguidor, String usuari) throws NotFoundException {
		try (Connection conn = cf.getConnection()) {

			Set<String> seguidors = this.getSeguidors(usuari);

			if (this.existeix(seguidor) && this.existeix(usuari) && !seguidors.contains(seguidor)) {
				try (PreparedStatement stmt = conn
						.prepareStatement("INSERT INTO seguidors (seguit, seguidor) VALUES (?,?)")) {
					stmt.setString(1, usuari);
					stmt.setString(2, seguidor);
					stmt.executeUpdate();

				}

			} else {
				throw new NotFoundException("Error, no es permet aquestos parametres");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void deixarDeSeguir(String seguidor, String usuari) throws NotFoundException {
		try (Connection conn = cf.getConnection()) {

			Set<String> seguits = this.getSeguits(seguidor);

			if (this.existeix(seguidor) && this.existeix(usuari) && seguits.contains(usuari)) {
				try (PreparedStatement stmt = conn
						.prepareStatement("DELETE FROM seguidors WHERE seguit = ? AND seguidor = ?")) {
					stmt.setString(1, usuari);
					stmt.setString(2, seguidor);
					stmt.executeUpdate();
				}

			} else {
				throw new NotFoundException("Error, no es permet aquestos parametres");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void deixarDeSeguir(String seguidor, String usuari, Connection conn) throws NotFoundException {

		Set<String> seguits = this.getSeguits(seguidor);

		if (this.existeix(seguidor) && this.existeix(usuari) && seguits.contains(usuari)) {
			try (PreparedStatement stmt = conn
					.prepareStatement("DELETE FROM seguidors WHERE seguit = ? AND seguidor = ?")) {
				stmt.setString(1, usuari);
				stmt.setString(2, seguidor);
				stmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			}

		} else {
			throw new NotFoundException("Error, no es permet aquestos parametres");
		}

	}

	@Override
	public Set<String> getSeguidors(String usuari) {
		Set<String> list = new HashSet<>();

		if (this.existeix(usuari)) {
			try (Connection conn = cf.getConnection()) {
				try (PreparedStatement stmt = conn
						.prepareStatement("SELECT seguidor FROM seguidors WHERE seguit = ?")) {
					stmt.setString(1, usuari);

					ResultSet rs = stmt.executeQuery();

					while (rs.next()) {
						list.add(rs.getString("seguidor"));
					}

				}

			} catch (SQLException e) {

				e.printStackTrace();
			}
		}

		return list;
	}

	@Override
	public Set<String> getSeguits(String usuari) {
		Set<String> list = new HashSet<>();

		if (this.existeix(usuari)) {
			try (Connection conn = cf.getConnection()) {
				try (PreparedStatement stmt = conn
						.prepareStatement("SELECT seguit FROM seguidors WHERE seguidor = ?")) {
					stmt.setString(1, usuari);

					ResultSet rs = stmt.executeQuery();

					while (rs.next()) {
						list.add(rs.getString("seguit"));
					}

				}

			} catch (SQLException e) {

				e.printStackTrace();
			}
		}

		return list;
	}

}
