package m03uf6.impl.followb;

import java.sql.Connection;
import java.sql.SQLException;

import com.mysql.cj.jdbc.MysqlDataSource;

import lib.db.ConnectionFactory;
import lib.db.DAOException;

public class ConnectionFactoryImpl implements ConnectionFactory{

	private MysqlDataSource ds;

	public ConnectionFactoryImpl() {
		ds = new MysqlDataSource();
		ds.setURL("jdbc:mysql://localhost:3306/dam2db");
		ds.setUser("root");
		ds.setPassword("super3");
		
		try {
			ds.setDumpQueriesOnException(true);
			ds.setServerTimezone("UTC"); // si hi ha problemes
			
		} catch (SQLException e) {
			throw new DAOException("setting datasource", e);
		}

	}

	@Override
	public Connection getConnection() throws SQLException {
		return ds.getConnection();
	}

}
