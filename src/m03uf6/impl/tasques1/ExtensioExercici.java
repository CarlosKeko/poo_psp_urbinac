package m03uf6.impl.tasques1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import lib.db.ConnectionFactory;
import m03uf6.prob.tasques1.TasquesExercici;

public class ExtensioExercici extends TasquesExercici{

	public static void main(String[] args) throws SQLException {
		new ExtensioExercici().run();
		
	}
	
	@Override
	public List<Tasca> searchPendents(ConnectionFactory cf) throws SQLException {
		
		List<Tasca> list = new ArrayList<>();
		
		try (Connection conn = cf.getConnection()) {
			
			try (Statement statement = conn.createStatement()) {
				
				try (ResultSet result = statement.executeQuery("SELECT * FROM tasques WHERE finalitzada = 0")) {
					
		            while (result.next()) {
		            	
		            	Tasca tasca = new Tasca();

		                tasca.id = result.getInt("id");
		                
		                tasca.descripcio = result.getString("descripcio");
		                tasca.dataInici = result.getDate("data_inici");
		                tasca.dataFinal = result.getDate("data_final");
		                tasca.finalitzada = result.getBoolean("finalitzada");
		                
		                list.add(tasca);
		            }
				}
			}
        }
		
		return list;
	}

	@Override
	public List<Tasca> searchByDescripcio(ConnectionFactory cf, String text) throws SQLException {
		
		List<Tasca> list = new ArrayList<>();
		
		try (Connection conn = cf.getConnection()) {
			
			try (PreparedStatement statement = conn.prepareStatement("SELECT * FROM tasques WHERE descripcio LIKE ?")) {
				statement.setString(1, "%"+ text + "%");
				try (ResultSet result = statement.executeQuery()) {
					
		            while (result.next()) {
		            	
		            	Tasca tasca = new Tasca();

		                tasca.id = result.getInt("id");
		                
		                tasca.descripcio = result.getString("descripcio");
		                tasca.dataInici = result.getDate("data_inici");
		                tasca.dataFinal = result.getDate("data_final");
		                tasca.finalitzada = result.getBoolean("finalitzada");
		                
		                list.add(tasca);
		            }
				}
			}
        }
		
		return list;
	}

	@Override
	public ConnectionFactory getConnectionFactory() throws SQLException {
		
        String url = "jdbc:mysql://localhost:3306/dam2db";
        Properties props = new Properties();
        props.setProperty("user", "myuser");
        props.setProperty("password", "mypassword");
        props.setProperty("dumpQueriesOnException", "true");
        props.setProperty("serverTimezone", "UTC"); // si hi ha problemes
		
        return () -> DriverManager.getConnection(url, props);
	}

}
