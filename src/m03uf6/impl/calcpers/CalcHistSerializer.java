package m03uf6.impl.calcpers;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import lib.JSONSerializer;

public class CalcHistSerializer implements JSONSerializer<CalcHistoric> {

	private List<String> historic = new ArrayList<String>();

	@Override
	public CalcHistoric deserialize(JSONObject json) {
		for (Object d : json.getJSONArray("Historic")) {
			this.historic.add((String) d);
		}
		
		return new CalcHistoric(historic, json.getInt("Index"));
	}

	@Override
	public JSONObject serialize(CalcHistoric t) {
		return new JSONObject().put("Historic", t.getHistoric()).put("Index", t.index());
	}

}
