package m03uf6.impl.calcpers;

import java.io.File;

import org.json.JSONObject;

import lib.TextFileStorage;
import m03uf5.impl.calcbox.ExprAvaluator2;

public class CalcHistPresenterImpl implements CalcHistPresenter {

	private CalcHistoric h;	
	private ExprAvaluator2 ea;
	private CalcHistView v;
	private CalcHistSerializer serializer;
	
	public CalcHistPresenterImpl() {
		try {
			ea = new ExprAvaluator2();
			serializer = new CalcHistSerializer();
			
			if (new File("historic.json").exists()) {
				TextFileStorage tf = new TextFileStorage();
				h = serializer.deserialize(new JSONObject(tf.readFromFile("historic.json")));
			}else {
				h = new CalcHistoric();
			}

			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		} 
	}
	
	@Override
	public void setView(CalcHistView v) {
		this.v = v;
		this.v.mostraEnrere(h.index() > 0);
		this.v.mostraEndavant(h.index() + 1 < h.size());	
		this.v.mostraNeteja(h.size() > 0);
		
		if (h.index() > -1) {
			calculaIMostra(h.current(), false);
		}
		
	}

	@Override
	public void calculaClic(String expr) {
		calculaIMostra(expr, true);	
	}

	@Override
	public void netejaClic() {
		h.clear();
		v.mostraEnrere(false);
		v.mostraEndavant(false);	
		v.mostraNeteja(false);
		v.setMostra("");
	}

	public void stop () {
		try {
			JSONObject json1 = serializer.serialize(this.h);
			TextFileStorage tfs = new TextFileStorage();
			tfs.writeToFile("historic.json", json1.toString());	
			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
	}
	
	@Override
	public void enrereClic() {		
		h.prev();
		calculaIMostra(h.current(), false);
	}

	@Override
	public void endavantClic() {			
		h.next();
		calculaIMostra(h.current(), false);
	}
	
	private void calculaIMostra(String expr, boolean afegir) {
	
		double valor;
		try {
			valor = ea.avalua(expr);
			if (afegir) {
				h.add(expr);
			}			
		} catch (Exception e) {			
			v.setMostra("error!");
			return;
		}
				
		v.setInput(expr);
		v.setMostra(Double.toString(valor));
		v.mostraEnrere(h.index() > 0);
		v.mostraEndavant(h.index() + 1 < h.size());	
		v.mostraNeteja(h.size() > 0);
	} 
	
}