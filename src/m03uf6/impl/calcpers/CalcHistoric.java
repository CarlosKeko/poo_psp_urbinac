package m03uf6.impl.calcpers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CalcHistoric implements Iterable<String> {

	private List<String> historic;
	private int idx;
	
	public CalcHistoric() {
		this(new ArrayList<>(), -1);
	}
	
	public CalcHistoric(List<String> exps, int idx) {
		this.historic = exps;
		this.idx = idx;
	}
	
	public CalcHistoric(CalcHistoric h) {
		this.historic = h.historic;
		this.idx = h.idx;
	}
	
	public void add(String exp) {
		historic.add(exp);
		idx = historic.size() - 1;
	}
	
	public int size() {
		return historic.size();
	}
	
	public int index() {
		return idx;
	}
	
	@Override
	public Iterator<String> iterator() {
		return historic.iterator();
	}
	
	public void prev() {		
		if (idx < 1) {
			throw new IllegalArgumentException("enrere?");
		}
		
		idx --;
	}
	
	public void next() {
		if (idx + 1 == historic.size()) {
			throw new IllegalArgumentException("endavant?");
		}		
		idx ++;
	}
	
	public String current() {
		if (idx == -1) {
			return null;
		}
		
		return historic.get(idx);
	}
	
	public void clear() {
		historic.clear();
		idx = - 1;
	}
	
	public List<String> getHistoric() {
		return this.historic;
	}
}
