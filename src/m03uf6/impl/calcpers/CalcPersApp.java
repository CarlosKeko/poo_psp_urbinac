package m03uf6.impl.calcpers;

import javafx.application.Application;
import javafx.stage.Stage;

public class CalcPersApp extends Application{

	public void start(Stage primaryStage) throws Exception {
		
		//Esta clase en mi opinion no hace falta porque CalcHistApp ya hace esta funci�n, pero en el anunciado pone que la hagamos y la pongo por si acaso.
		
		CalcHistView v = new CalcHistViewImpl(primaryStage);
		CalcHistPresenter p = new CalcHistPresenterImpl();
		
		p.setView(v);
		v.setPresenter(p);
	}

	public static void main(String[] args) {
		launch(args);
	}

}
