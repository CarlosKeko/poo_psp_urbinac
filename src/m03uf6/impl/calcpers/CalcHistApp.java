package m03uf6.impl.calcpers;

import javafx.application.Application;
import javafx.stage.Stage;

public class CalcHistApp extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		CalcHistView v = new CalcHistViewImpl(primaryStage);
		CalcHistPresenter p = new CalcHistPresenterImpl();
		
		p.setView(v);
		v.setPresenter(p);
	}

	public static void main(String[] args) {
		launch(args);
	}
}

interface CalcHistView {
	
	void setPresenter(CalcHistPresenter p);
	
	void setInput(String display);
	void setMostra(String display);
	void mostraEnrere(boolean b);
	void mostraEndavant(boolean b);
	void mostraNeteja(boolean b);
}

interface CalcHistPresenter {
	
	void setView(CalcHistView v);

	void calculaClic(String text);
	void enrereClic();
	void endavantClic();
	void netejaClic();
	void stop();
}