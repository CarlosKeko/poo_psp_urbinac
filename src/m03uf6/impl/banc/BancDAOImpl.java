package m03uf6.impl.banc;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

import lib.db.ConnectionFactory;
import lib.db.DuplicateException;
import lib.db.NotFoundException;
import m03uf6.prob.banc.BancDAO;
import m03uf6.prob.banc.BancDB;
import m03uf6.prob.banc.Moviment;
import m03uf6.prob.banc.SenseFonsException;

public class BancDAOImpl implements BancDAO{

	private ConnectionFactory cf;
	
	public BancDAOImpl(ConnectionFactory connec) {
		this.cf = connec;
	}
	
	
	public void init() {
		BancDB.init(cf);
		
	}

	@Override
	public void nouCompte(int numero, BigDecimal saldoInicial) throws DuplicateException {
		try (Connection conn = cf.getConnection()) {
			try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO compte (id_compte, saldo) VALUES (?,?)")) {
				stmt.setInt(1, numero);
				stmt.setBigDecimal(2, saldoInicial);
				stmt.executeUpdate();
				
			}
			
		} catch (SQLException e) {
			if (e.getErrorCode() == 1062) {
				throw new DuplicateException("ID duplicada");
				
			}else {
				e.printStackTrace();
				
			}
		}
	}

	@Override
	public int transferir(int origen, int desti, BigDecimal quantitat) throws NotFoundException, SenseFonsException {
		try (Connection conn = cf.getConnection()) {
			conn.setAutoCommit(false);
			
			try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO moviment (id_compte_origen, id_compte_desti, quantitat) VALUES (?,?,?)")) {
				stmt.setInt(1, origen);
				stmt.setInt(2, desti);
				stmt.setBigDecimal(3, quantitat);
				
				try (PreparedStatement stmtUpdate1 = conn.prepareStatement("UPDATE compte SET saldo = ? WHERE id_compte = ?")) {
					BigDecimal bg = new BigDecimal("0");

					if (bg.compareTo(this.getSaldo(origen).subtract(quantitat)) == -1) {
						stmtUpdate1.setBigDecimal(1, this.getSaldo(origen).subtract(quantitat));
						stmtUpdate1.setInt(2, origen);
						stmtUpdate1.executeUpdate();
					
					}else {
						conn.rollback();
						throw new SenseFonsException("No hi ha fons");
					}
				}
				
				try (PreparedStatement stmtUpdate2 = conn.prepareStatement("UPDATE compte SET saldo = ? WHERE id_compte = ?")) {
					stmtUpdate2.setBigDecimal(1, this.getSaldo(desti).add(quantitat));
					stmtUpdate2.setInt(2, desti);
					stmtUpdate2.executeUpdate();
				}
				
				stmt.executeUpdate();
				
			}
			
			conn.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return 0;
	}

	@Override
	public List<Moviment> getMoviments(int origen) {
		List<Moviment> list = new ArrayList<>();
		
		try (Connection conn = cf.getConnection()) {
			try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM `moviment` WHERE id_compte_origen = ? OR id_compte_desti = ? ORDER BY id_moviment ASC")) {
				stmt.setInt(1, origen);
				stmt.setInt(2, origen);
				
				ResultSet rs = stmt.executeQuery();
				
				while (rs.next()) {
	            	
					Moviment moviment = new Moviment();

					moviment.origen = rs.getInt("id_compte_origen");
					moviment.desti = rs.getInt("id_compte_desti");
					moviment.quantitat = rs.getBigDecimal("quantitat");
	                
	                list.add(moviment);
	            }
				
			}
			
		}catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		return list;
	}

	@Override
	public BigDecimal getSaldo(int compte) throws NotFoundException {
		
		try (Connection conn = cf.getConnection()) {
			try (PreparedStatement stmt = conn.prepareStatement("SELECT saldo FROM compte WHERE id_compte = ?")) {
				stmt.setInt(1, compte);
				
				ResultSet rs = stmt.executeQuery();
				
				if (rs.next()) {
					return rs.getBigDecimal("saldo");
					
				}else {
					throw new NotFoundException("No hi ha resultat");
				}
				
			}
			
		}catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		return null;
	}

}
