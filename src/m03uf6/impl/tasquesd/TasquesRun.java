package m03uf6.impl.tasquesd;

import java.util.List;
import java.util.logging.Logger;

import lib.LoggingConfigurator;
import lib.Problems;
import lib.Reflections;
import lib.db.ConnectionFactory;
import m03uf6.prob.tasquesd.TascaTO;
import m03uf6.prob.tasquesd.TasquesDAO;

public class TasquesRun {
	
	private static final Logger LOGGER = Logger.getLogger(TasquesRun.class.getName());

	public static void main(String[] args) {
		
		LoggingConfigurator.configure();
		
		String packageName = Problems.getImplPackage(TasquesDAO.class);
		
		// cf = new ConnectionFactoryImpl()
		ConnectionFactory cf = Reflections.newInstanceOfType(
				ConnectionFactory.class, packageName + ".ConnectionFactoryImpl");

		// dao = new TasquesDAOImpl(cf)		
		TasquesDAO dao = Reflections.newInstanceOfType(
				TasquesDAO.class, packageName + ".TasquesDAOImpl", 
				new Class[] {ConnectionFactory.class}, 
				new Object[] {cf});
		
		dao.init(true);

		List<TascaTO> tasques = dao.findAll();
		for (TascaTO tasca: tasques) {
			LOGGER.info(tasca.toString());
		}
	}
}
