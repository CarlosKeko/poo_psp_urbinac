package m03uf6.impl.tasquesd;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import lib.db.ConnectionFactory;
import lib.db.NotFoundException;
import m03uf6.prob.tasquesd.TascaTO;
import m03uf6.prob.tasquesd.TasquesDAO;
import m03uf6.prob.tasquesd.TasquesDB;

public class TasquesDAOImpl implements TasquesDAO{
	
	private ConnectionFactory cf;
	
	public TasquesDAOImpl(ConnectionFactory connec) {
		this.cf = connec;
	}

	@Override
	public TascaTO find(Integer id) throws NotFoundException {
		try (Connection conn = cf.getConnection()) {
			
			try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM tasques WHERE id = ?")) {
				if(id == 0) id = 1; //La primary key no deja poner valor 0, ademas de ser una mala practica para cietos lenguajes de programación por ejemplo en php asi que hago esto.
				stmt.setInt(1, id);
				ResultSet rs = stmt.executeQuery();
				
				TascaTO tasca = null;
				
				while (rs.next()) {
					tasca = new TascaTO();
					tasca.descripcio = rs.getString("descripcio");
					tasca.id = rs.getInt("id");
					tasca.dataInici = rs.getDate("data_inici");
					tasca.dataFinal = rs.getDate("data_final");
					tasca.finalitzada = rs.getBoolean("finalitzada");
				}
				
				if (tasca != null) {
					return tasca;
					
				}else {
					throw new NotFoundException("No se ha encontrado el registro");
				}

			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}

	@Override
	public void delete(Integer id) throws NotFoundException {
		try (Connection conn = cf.getConnection()) {
			
			if (this.find(id) != null) {
				try (PreparedStatement stmt = conn.prepareStatement("DELETE FROM tasques WHERE id = ?")) {
					stmt.setInt(1, id);
					stmt.executeUpdate();
				}
				
			}else {
				throw new NotFoundException("No se ha encontrado el registro");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Integer create(TascaTO tasca) {
		
		try (Connection conn = cf.getConnection()) {
			
			try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO tasques VALUES (default,?,?,?,?)")) {
				if(tasca.id == 0) tasca.id = 1; //La primary key no deja poner valor 0, ademas de ser una mala practica para cietos lenguajes de programación por ejemplo en php asi que hago esto.
				stmt.setString(1, tasca.descripcio);
				stmt.setDate(2, (Date) tasca.dataInici);
				stmt.setDate(3, (Date) tasca.dataFinal);
				stmt.setBoolean(4,tasca.finalitzada);

				return stmt.executeUpdate();
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public void update(TascaTO tasca) throws NotFoundException {
		try (Connection conn = cf.getConnection()) {
			
			if (this.find(tasca.id) != null) {
				try (PreparedStatement stmt = conn.prepareStatement("UPDATE tasques SET descripcio = ?, data_inici = ?, data_final = ?, finalitzada = ? WHERE id = ?")) {
					stmt.setString(1, tasca.descripcio);
					stmt.setDate(2, (Date) tasca.dataInici);
					stmt.setDate(3, (Date) tasca.dataFinal);
					stmt.setBoolean(4, tasca.finalitzada);
					stmt.setInt(5, tasca.id);
					
					stmt.executeUpdate();
					
				}
			}else {
				throw new NotFoundException("No se ha encontrado el registro");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public List<TascaTO> findAll() {
		List<TascaTO> list = new ArrayList<>();
		
		try (	Connection conn = cf.getConnection();
				Statement statement = conn.createStatement();
				ResultSet result = statement.executeQuery("SELECT * FROM tasques")) {
		
			while (result.next()) {
            	
				TascaTO tasca = new TascaTO();

                tasca.id = result.getInt("id");
                
                tasca.descripcio = result.getString("descripcio");
                tasca.dataInici = result.getDate("data_inici");
                tasca.dataFinal = result.getDate("data_final");
                tasca.finalitzada = result.getBoolean("finalitzada");
                
                list.add(tasca);
            }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}

	@Override
	public void init(boolean withData) {
		TasquesDB.init(this.cf, withData);
		
	}
	
}
