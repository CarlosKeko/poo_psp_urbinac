package m09uf3.impl.chatroom;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import lib.Startable;

public class ChatRoomServerImpl implements Startable, Runnable{

	private String host;
	private int port;
	private Thread thread;
	private DatagramSocket socket;
	private volatile ArrayList<Usuario> usuarisConectats;
	
	static final int SIZE = 512;
	
	static final Logger LOGGER = Logger.getLogger(ChatRoomServerImpl.class.getName());
	
	public ChatRoomServerImpl(String host, int port) {
		this.host = host;
		this.port = port;
		usuarisConectats = new ArrayList<Usuario>();
		thread = new Thread(this, "fillServidor");
	}

	public void start() {
		thread.start();
		
	}

	public void stop() {
		thread.interrupt();
		socket.close();
	}

	public boolean isStarted() {
		return thread.isAlive();
	}
	
	static void handle(DatagramSocket socket, DatagramPacket packet, ArrayList<Usuario> usuarisConectats) {
		byte[] entryPacket = packet.getData();
			
		switch (entryPacket[0]) {
			case 1:
				byte[] packetUsername = new byte[entryPacket[1]];
				
				for (int i = 0; i < packetUsername.length; i++) {
					packetUsername[i] = entryPacket[i + 2];
				}
				
				String nomUsu = new String(packetUsername, 0, packetUsername.length);	
				usuarisConectats.add(new Usuario(nomUsu, packet.getAddress(), packet.getPort()));	
				break;
				
			case 2:
				byte[] packetMessage = new byte[entryPacket[1]];

				for (int i = 0; i < packetMessage.length; i++) {
					packetMessage[i] = entryPacket[i + 2];
				}
				
				String username = null;

				for (Usuario usuario : usuarisConectats) {
					if (usuario.getAddress().equals(packet.getAddress()) && usuario.getPort() == packet.getPort()) {
						username = usuario.getUsername();
					}
				}
				
				byte[] bytesAEnviar = new byte[username.length() + packetMessage.length + 3];
				bytesAEnviar[0] = 1;
				bytesAEnviar[1] = (byte) username.length();
				bytesAEnviar[username.length() + 2] = (byte) packetMessage.length;
				
				byte[] userNameBytes = username.getBytes(StandardCharsets.UTF_8);
				
				for (int i = 0; i < username.length(); i++) {
					bytesAEnviar[i + 2] = userNameBytes[i]; 
				}
				
				for (int i = 0; i < packetMessage.length; i++) {
					bytesAEnviar[i + username.length() + 3] = packetMessage[i];
				}
				
				for (Usuario usuario : usuarisConectats) {
					
					try {
						packet = new DatagramPacket(bytesAEnviar, bytesAEnviar.length, usuario.getAddress(), usuario.getPort());
						socket.send(packet);
						
					} catch (SocketException e) {
						LOGGER.log(Level.SEVERE, null, e);	
					} catch (IOException e) {
						LOGGER.log(Level.SEVERE, null, e);	
					}
					
				}
				
				break;
				
			case 3:
				for (Usuario usuario : usuarisConectats) {
					if (usuario.getAddress().equals(packet.getAddress()) && usuario.getPort() == packet.getPort()) {
						usuarisConectats.remove(usuario);
						break;
					}
				}
				break;
		}
	}
	
	public void run() {
		try {
			socket = new DatagramSocket(port, InetAddress.getByName(host));
			socket.setSoTimeout(10000);
			
			while (true) {
				DatagramPacket packet = new DatagramPacket(new byte[SIZE], SIZE);				
				try {
					socket.receive(packet);					
					handle(socket, packet, usuarisConectats);
					
				} catch (SocketTimeoutException  e) {
					LOGGER.fine("timeout receiving!");
				}
			}
			
		}catch (IOException e) {		
			if (socket.isClosed()) {
				return;
			}
			
			LOGGER.log(Level.SEVERE, null, e);	
		} 
	}
	
	public static class Usuario {
		
		private String username;
		private InetAddress address;
		private int port;
		
		public Usuario(String username, InetAddress address, int port) {
			this.username = username;
			this.address = address;
			this.port = port;
		}
		 
		public int getPort() {
			return port;
		}
		
		public InetAddress getAddress() {
			return address;
		}
		
		public String getUsername() {
			return username;
		}
	}

}
