package m09uf3.impl.chatroom;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

import m09uf3.prob.chatroom.ChatRoomClient;
import m09uf3.prob.chatroom.ChatRoomHandler;

public class ChatRoomClientImpl implements ChatRoomClient, Runnable{

	private String host;
	private int port;
	private Thread thread;
	private String username;
	private ChatRoomHandler handler;
	private DatagramSocket socket;
	private InetAddress address;
	
	static final Logger LOGGER = Logger.getLogger(ChatRoomClientImpl.class.getName());
	static final Charset UTF8 = StandardCharsets.UTF_8;
	static final Charset CHARSET = UTF8;
	
	static final int SIZE = 512;
	
	public ChatRoomClientImpl(String host, int port) {
		this.host = host;
		this.port = port;
	}
	
	public void connect(String username, ChatRoomHandler handler) throws IOException {
		this.username = username;
		this.handler = handler;
		thread = new Thread(this, username);
		thread.start();
		
	}

	public void message(String message) throws IOException {
		byte[] buf = new byte[message.getBytes(CHARSET).length + 2];
		buf[0] = (byte) 2;
		buf[1] = (byte) message.getBytes(CHARSET).length;
		byte[] fileNameArray = message.getBytes(StandardCharsets.UTF_8);
		
		for (int i = 0; i < fileNameArray.length; i++) {
			buf[i + 2] = fileNameArray[i];
		}
		
		DatagramPacket packet = new DatagramPacket(buf, buf.length, address, port);
		socket.send(packet);
		
	}

	public void disconnect() throws IOException {
		byte[] buf = new byte[] {3};
		
		DatagramPacket packet = new DatagramPacket(buf, buf.length, address, port);
		socket.send(packet);
		thread.interrupt();
		socket.close();
	}

	public void run() {
		try {
			socket = new DatagramSocket();
			socket.setSoTimeout(3000);
			address = InetAddress.getByName(host);
			
			byte[] buf = new byte[username.length() + 2];
			buf[0] = (byte) 1;
			buf[1] = (byte) username.length();
			
			byte[] fileNameArray = username.getBytes(StandardCharsets.UTF_8);
			
			for (int i = 0; i < fileNameArray.length; i++) {
				buf[i + 2] = fileNameArray[i];
			}

			DatagramPacket packet = new DatagramPacket(buf, buf.length, address, port);
			socket.send(packet);
			
			while (true) {
				try {
					packet = new DatagramPacket(new byte[SIZE], SIZE);
					socket.receive(packet);
					
					byte[] mensajeServidor = packet.getData();
					
					byte[] packetNom = new byte[mensajeServidor[1]];
					int aux = packetNom.length;
					byte[] packetMensaje = new byte[mensajeServidor[aux + 2]];
					
					if (mensajeServidor[0] == 1) {
						for (int i = 0; i < packetNom.length; i++) {
							packetNom[i] = mensajeServidor[i + 2];
						}
						
						for (int i = 0; i < packetMensaje.length; i++) {
							packetMensaje[i] = mensajeServidor[i + aux + 3];

						}
						
						handler.message(new String(packetNom, 0, packetNom.length), new String(packetMensaje, 0, packetMensaje.length));
					}
				}catch (SocketTimeoutException e) {
					LOGGER.fine("cuidao");
				}
				
			}
			
		} catch (SocketException e) {
			LOGGER.fine("ending");
			
		} catch (UnknownHostException e) {
			LOGGER.log(Level.SEVERE, null, e);
			
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, null, e);
		}
		
	}

}
