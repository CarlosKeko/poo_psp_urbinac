package m09uf3.impl.guessb;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import lib.Startable;
import m09uf3.prob.guessb.GuessSecret;

public class GuessServerImpl implements Startable, Runnable{

	private String host;
	private int port;
	private Thread thread; 
	private ServerSocket serverSocket;
	private volatile boolean closed;
	private GuessSecret guessSecret;
	
	static final Logger LOGGER = Logger.getLogger(GuessServerImpl.class.getName());

	static final Charset CHARSET = StandardCharsets.UTF_8;
	static final int TIMEOUT_MILLIS = 1500;
	static final int SIZE = 512;
	
	public GuessServerImpl(String host, int port, GuessSecret gs) {
		this.host = host;
		this.port = port;
		this.guessSecret = gs;
		
	}
	
	public void start() {
		this.thread = new Thread(this, "guessServer");
		this.thread.start();
		
	}

	public void stop() {
		try {
			closed = true;
			serverSocket.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}	
	}

	public boolean isStarted() {
		return thread != null && thread.isAlive();
	}
	
	private void handle(Socket clientSocket) {
		
		SocketAddress remote = clientSocket.getRemoteSocketAddress();					
		LOGGER.info("accept from " + remote);
		
		try {	
			InputStream in = clientSocket.getInputStream();
			OutputStream out = clientSocket.getOutputStream();
			
			int numeroMaxim = guessSecret.upper();
			int numSecret = guessSecret.next();
			
			out.write(1);
			out.write((byte) numeroMaxim);
			
			jugarJoc(in, out, remote, numSecret);
			
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "handling from " + remote, e);
			
		} finally {
			try {
				clientSocket.close();
				
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	private void jugarJoc(InputStream in, OutputStream out, SocketAddress address, int numSecret) {
		
		try {
			while (true) {
				try {	
					int entradaClient = in.read();
					
					if (entradaClient == -1) {
						LOGGER.info("end of stream!");
						break;
						
					}else {	
						if (entradaClient > numSecret) {
							out.write(1);
							
						}else if (entradaClient < numSecret) {
							out.write(2);
							
						}else {
							out.write(3);
							break;
							
						}			
					}	
					
				} catch (SocketTimeoutException e) {
					LOGGER.fine("timeout waiting");
					
				} catch (IOException e) {
					LOGGER.log(Level.SEVERE, null, e);
				}
			}
			
		}finally {
			LOGGER.info("stopping game for " + address);
			
		}
		
	}
	
	public void run() {
		
		ExecutorService service = Executors.newCachedThreadPool();
		
		try {
			serverSocket = new ServerSocket(port, 10, InetAddress.getByName(host));
			serverSocket.setSoTimeout(TIMEOUT_MILLIS);
			
			while (!closed) {
				
				Socket clientSocket = serverSocket.accept();
				clientSocket.setSoTimeout(TIMEOUT_MILLIS);										
				service.execute(() -> handle(clientSocket));
			}
			
		} catch (IOException e) {
			if (closed) {
				LOGGER.info("closed reason: " + e.getMessage());
			}
			else {
				LOGGER.log(Level.SEVERE, "accepting", e);	
			}
			
		} finally {
			service.shutdown();
		}	
	}

}
