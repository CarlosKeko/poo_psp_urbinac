package m09uf3.impl.guessb;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

import m09uf3.prob.guessb.GuessClient;
import m09uf3.prob.guessb.GuessStatus;

public class GuessClientImpl implements GuessClient{

	private String host;
	private int port;
	private Socket socket;
	private OutputStream os;
	private InputStream in;
	
	static final Logger LOGGER = Logger.getLogger(GuessClientImpl.class.getName());
	static final Charset UTF8 = StandardCharsets.UTF_8;
	static final Charset CHARSET = UTF8;
	
	public GuessClientImpl(String host, int port) {
		this.socket = new Socket();
		this.host = host;
		this.port = port;
	}
	
	public byte[] start() {
		try {
			socket.connect(new InetSocketAddress(host, port), 1500);
			socket.setSoTimeout(10000);
			os = socket.getOutputStream();
			in = socket.getInputStream();
			
			byte bytes[] = new byte[2];
			
			for (int i = 0; i < bytes.length; i++) {
				
				try {
					byte numero = (byte) in.read();
					
					if (numero == -1) {
						socket.close();
						break;
						
					}else {
						bytes[i] = (byte) numero;
					}
					
				}catch (SocketTimeoutException e) {
					LOGGER.fine("Esperando respuesta");
				}
			}
			
			return bytes;
			
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, null, e);
			throw new RuntimeException(e);
		}
	}

	public GuessStatus play(byte play) {
		
		try {
			os.write(play);
			
			switch (in.read()) {
			
			case 1: 
				System.out.println("Entrado 1");
				return GuessStatus.SMALLER;
			
			case 2:
				System.out.println("Entrado 2");
				return GuessStatus.LARGER;
				
			case 3: 
				System.out.println("Entrado 3");
				socket.close();
				return GuessStatus.EXACT;
			
			default:
				throw new RuntimeException();
			}
			
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, null, e);
			throw new RuntimeException(e);	
		}
	}

}
