package m09uf3.impl.bancapi;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import lib.db.DuplicateException;
import lib.db.NotFoundException;
import lib.http.HTTPProcessor;
import lib.http.HTTPRequest;
import lib.http.HTTPResponse;
import lib.http.HTTPUtils;
import lib.http.SocketHTTPServer;
import m03uf6.prob.banc.BancDAO;
import m03uf6.prob.banc.Moviment;
import m03uf6.prob.banc.SenseFonsException;

public class BancAPIProcessorImpl implements HTTPProcessor{

	static final Logger LOGGER = Logger.getLogger(BancAPIProcessorImpl.class.getName());
	
	static final String HOST = "localhost";
	static final int PORT = 8888;	
	static final String BASEURL = "http://" + HOST + ":" + PORT;
	
	private BancDAO banc;
	
	public BancAPIProcessorImpl(BancDAO banc) {
		this.banc = banc;
	}
	
	public void process(HTTPRequest req, HTTPResponse res) {
		
		if ("/compte".equals(req.getPath()) && "POST".equals(req.getMethod())) {	
			LOGGER.info("Entrando por POST /compte");
			peticioNouCompte(req, res);
	
		}else if("/transferir".equals(req.getPath()) && "PUT".equals(req.getMethod())) {
			LOGGER.info("Entrando por PUT /transferir");
			peticioTransferenciaCompte(req, res);
			
			
		}else if(req.getPath().length() > 11 && "/moviments/".equals(req.getPath().substring(0, 11)) && "GET".equals(req.getMethod())) {
			LOGGER.info("Entrando por GET /moviments/{id}");
			peticioMoviments(req, res);
			
			
		}else if(req.getPath().length() > 7 && "/saldo/".equals(req.getPath().substring(0, 7)) && "GET".equals(req.getMethod())) {
			LOGGER.info("Entrando por GET /saldo/{id}");
			peticioSaldo(req, res);
			
		}else {
			LOGGER.severe("Se ha recibidio una entrada no valida");
			process(res, "400 Bad Request", null);
			
		}	
	}
	
	private void process(HTTPResponse res, String status, String jsonStr) {
		
		PrintWriter out = res.getWriter();
		
		out.println("HTTP/1.1 " + status);
		out.println("Server: CarlosUrbina");
		out.println("Date: " + HTTPUtils.toHTTPDate(new Date()));
		if (jsonStr != null) {
			out.println("Content-type: application/json; charset=" + SocketHTTPServer.CHARSET);
			out.println("Content-length: " + jsonStr.getBytes(SocketHTTPServer.CHARSET).length);
		}
		out.println(); // blank line!
		if (jsonStr != null) {
			out.println(jsonStr);
		}
	}
	
	private void peticioNouCompte(HTTPRequest req, HTTPResponse res) {
		String body = HTTPUtils.readToString(req.getReader());			
		JSONObject jin = new JSONObject(body);	
		
		try {
			banc.nouCompte(jin.getInt("numero"), jin.getBigDecimal("saldoInicial"));
			LOGGER.info("RECIBIDIO NUMERO: " + jin.getInt("numero") + ", RECIBIDO SALDO INCICIAL: " + jin.getBigDecimal("saldoInicial"));
			
			process(res, "201 Created", null);
			LOGGER.info("NOU COMPTE CREADO CORRECTAMENTE");
			
		}  catch (DuplicateException e) {
			process(res, "409 Conflict", null);	
			LOGGER.info("Error " + e.getMessage());
		}
	}
	
	private void peticioTransferenciaCompte(HTTPRequest req, HTTPResponse res) {
		String body = HTTPUtils.readToString(req.getReader());			
		JSONObject jin = new JSONObject(body);	
		
		try {
			int id = banc.transferir(jin.getInt("origen"), jin.getInt("desti"), jin.getBigDecimal("quantitat"));
			LOGGER.info("RECIBIDIO ORIGEN: " + jin.getInt("origen") + ", RECIBIDO DESTINO: " + jin.getInt("desti") + ", RECIBIDO LA CANTIDAD: " + jin.getBigDecimal("quantitat"));
			
			JSONObject jout = new JSONObject().put("id", id);
			process(res, "200 Ok", jout.toString());
			LOGGER.info("TRANSFERENCIA CREADA CORRECTAMENTE");
			
		} catch (SenseFonsException e) {
			process(res, "409 Conflict", null);	
			LOGGER.info("Error " + e.getMessage());
			
		}  catch (NotFoundException e) {
			process(res, "404 Not Found", null);	
			LOGGER.info("Error " + e.getMessage());
		}
	}
	
	private void peticioMoviments(HTTPRequest req, HTTPResponse res) {
		
		try {
			int numeroPeticio = Integer.parseInt(req.getPath().substring(11));
			
			List<Moviment> llistaMoviments = banc.getMoviments(numeroPeticio);
			JSONArray jsonArray = new JSONArray();
			
			for (Moviment moviment : llistaMoviments) {
				JSONObject jObject = new JSONObject();
				
				jObject.put("quantitat", moviment.quantitat);
				jObject.put("desti", moviment.desti);
				jObject.put("origen", moviment.origen);
				
				jsonArray.put(jObject);
			}
			
			JSONObject jout = new JSONObject().put("Array d’objectes", jsonArray);
			process(res, "200 Ok", jout.toString());
			
		}catch (NumberFormatException e) {
			LOGGER.severe("Se ha recibidio una entrada no valida");
			process(res, "400 Bad Request", null);
			
		}
	}
	
	private void peticioSaldo(HTTPRequest req, HTTPResponse res) {
		try {
			int numeroPeticio = Integer.parseInt(req.getPath().substring(7));
			
			BigDecimal saldo = banc.getSaldo(numeroPeticio);
			
			JSONObject jout = new JSONObject().put("saldo", saldo);
			process(res, "200 Ok", jout.toString());
			
		}catch (NumberFormatException e) {
			LOGGER.severe("Se ha recibidio una entrada no valida");
			process(res, "400 Bad Request", null);
			
		} catch (NotFoundException e) {
			process(res, "404 Not Found", null);	
			LOGGER.info("Error " + e.getMessage());
		}
	}

}
