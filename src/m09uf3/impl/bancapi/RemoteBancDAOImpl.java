package m09uf3.impl.bancapi;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;

import lib.db.DuplicateException;
import lib.db.NotFoundException;
import m03uf6.prob.banc.BancDAO;
import m03uf6.prob.banc.Moviment;
import m03uf6.prob.banc.SenseFonsException;

public class RemoteBancDAOImpl implements BancDAO {

	private String url;
	private JSONObject json;
	
	static final Logger LOGGER = Logger.getLogger(RemoteBancDAOImpl.class.getName());
	
	public RemoteBancDAOImpl(String url) {
		this.url = url;
	}
	
	public void init() {
		new RuntimeException("Metodo no valido");
		
	}

	public void nouCompte(int numero, BigDecimal saldoInicial) throws DuplicateException {
		try {
			json = new JSONObject().put("numero", numero);	
			json.put("saldoInicial", saldoInicial);
			
			Response res = Jsoup.connect(url + "/compte")
					.method(Method.POST)
					.requestBody(json.toString())
					.ignoreContentType(true).ignoreHttpErrors(true).execute();
			
			if (res.statusCode() == 201) {
				LOGGER.info("Cuenta creada correctamente! NUMERO: " + numero + ", SALDOINICIAL: " + saldoInicial);
				
			}else if (res.statusCode() == 409){
				LOGGER.info("Error! Numero de cuenta duplicado! STATUSCODE: " + res.statusCode());
				throw new DuplicateException("Numero de cuenta duplicado! NUMERO: " + numero);
				
			}else {
				LOGGER.info("Error! no se ha creado la cuenta STATUSCODE: " + res.statusCode());
				throw new RuntimeException("Error! Bad Request!");
			}
			
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "requesting", e);
			throw new RuntimeException("Error! Bad Request!");
		}
	}

	public int transferir(int origen, int desti, BigDecimal quantitat) throws NotFoundException, SenseFonsException {
		try {
			json = new JSONObject().put("origen", origen);	
			json.put("desti", desti);
			json.put("quantitat", quantitat);
			
			Response res = Jsoup.connect(url + "/transferir")
					.method(Method.PUT)
					.requestBody(json.toString())
					.ignoreContentType(true).ignoreHttpErrors(true).execute();
			
			if (res.statusCode() == 200) {
				LOGGER.info("Transferencia realizada correctamente! ORIGEN: " + origen + ", DESTI: " + desti + ", QUANTITAT: " + quantitat);
				return new JSONObject(res.body()).getInt("id");
				
			}else if (res.statusCode() == 404){
				LOGGER.info("Error! Not Found! " + res.statusCode());
				throw new NotFoundException("Error! Not Found! " + res.statusCode());
				
			}else if (res.statusCode() == 409){
				LOGGER.info("Error! Conflict sin fondos! CUENTA " + origen);
				throw new SenseFonsException("Error! Conflict sin fondos! " + res.statusCode());
				
			}else {
				LOGGER.info("Error! no se ha transferido STATUSCODE: " + res.statusCode());
				throw new RuntimeException("Error! Bad Request!");
			}
			
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "requesting", e);
			throw new RuntimeException("Error! Bad Request!");
		}
	}

	public List<Moviment> getMoviments(int origen) {
		
		try {
			Response res = Jsoup.connect(url + "/moviments/" + origen)
					.method(Method.GET)
					.ignoreContentType(true).ignoreHttpErrors(true).execute();
			
			if (res.statusCode() == 200) {
				LOGGER.info("Movimientos obtenidos correctamente! ORIGEN: " + origen);
				
				JSONArray jsoArray = new JSONObject(res.body()).getJSONArray("Array d’objectes");
				List<Moviment> moviments = new ArrayList<Moviment>();	
				
				for (int i = 0; i < jsoArray.length(); i++) {
					JSONObject object = jsoArray.getJSONObject(i);
					Moviment moviment = new Moviment();
					
					moviment.origen = object.getInt("origen");
					moviment.desti = object.getInt("desti");
					moviment.quantitat = object.getBigDecimal("quantitat");
					
					moviments.add(moviment);
					
				}
				
				return moviments;
				
			}else {
				LOGGER.info("Error! no se han obtenido los movimientos: " + res.statusCode());
				throw new RuntimeException("Error! Bad Request!");
			}
			
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "requesting", e);
			throw new RuntimeException("Error! Bad Request!");
			
		}
	}

	public BigDecimal getSaldo(int compte) throws NotFoundException {
		
		try {
			Response res = Jsoup.connect(url + "/saldo/" + compte)
					.method(Method.GET)
					.ignoreContentType(true).ignoreHttpErrors(true).execute();
			
			if (res.statusCode() == 200) {
				
				LOGGER.info("Saldo obtenido correctamente! CUENTA: " + compte);
				
				return new JSONObject(res.body()).getBigDecimal("saldo");
				
			}else if (res.statusCode() == 404) {
				throw new NotFoundException("Error! No se ha encontrado la cuenta!");
				
			}else {
				LOGGER.info("Error! no se ha obtenido el saldo: " + res.statusCode());
				throw new RuntimeException("Error! Bad Request!");
				
			}
			
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "requesting", e);
			throw new RuntimeException("Error! Bad Request!");
			
		}
	}
}
