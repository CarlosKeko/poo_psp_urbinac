package m09uf3.impl.httpupper;

import java.io.PrintWriter;
import java.util.Date;
import java.util.logging.Logger;

import org.json.JSONObject;

import lib.http.HTTPProcessor;
import lib.http.HTTPRequest;
import lib.http.HTTPResponse;
import lib.http.HTTPUtils;
import lib.http.SocketHTTPServer;

public class HTTPUpperProcessorImpl implements HTTPProcessor{
	
	static final Logger LOGGER = Logger.getLogger(HTTPUpperProcessorImpl.class.getName());
	
	public void process(HTTPRequest req, HTTPResponse res) {
		
		if ("/get".equals(req.getPath()) && "GET".equals(req.getMethod())) {
			LOGGER.info("Entrando por GET /get/?text={text}");
			peticioGet(req, res);
			
		}else if("/post".equals(req.getPath()) && "POST".equals(req.getMethod())) {
			LOGGER.info("Entrando por POST /post");
			peticioPostTextHTML(req, res);
			
		}else if("/json".equals(req.getPath()) && "POST".equals(req.getMethod())) {
			LOGGER.info("Entrando por POST /json");
			peticioPostJSON(req, res);
			
		}else {
			LOGGER.info("Error url");
			process(res, "404 Not Found", null, null, "Not Found");
		}
		
	}
	
	private void process(HTTPResponse res, String status, String jsonStr, String paraula, String error) {
		
		PrintWriter out = res.getWriter();
	
		out.println("HTTP/1.1 " + status);
		out.println("Server: CarlosUrbina");
		out.println("Date: " + HTTPUtils.toHTTPDate(new Date()));
		if (jsonStr != null) {
			out.println("Content-type: application/json; charset=" + SocketHTTPServer.CHARSET);
			out.println("Content-length: " + jsonStr.getBytes(SocketHTTPServer.CHARSET).length);
		}
		out.println(); // blank line!
		if (jsonStr != null) {
			out.println(jsonStr);
		}
		
		if (error != null) {
			String html = "<html><body><h1>" + error + "</h1></body></html>";
			out.println(html);
			out.flush();
		}
		
		if (paraula != null) {
			String html = "<html><body><h1 id=\"upper\">" + paraula + "</h1></body></html>";
			out.println(html);
			out.flush();
		}

	}
	
	private void peticioGet(HTTPRequest req, HTTPResponse res) {
		
		if (req.getQuery() != null && req.getQuery().split("=")[0].equals("text")) {		
			String paraula = req.getQuery().split("=")[1].toUpperCase();
			process(res, "200 ok", null, paraula, null);
			
		}else {
			process(res, "400 Bad Request", null, null, "Bad Request");
		}
	}
	
	private void peticioPostTextHTML(HTTPRequest req, HTTPResponse res) {
		String body = HTTPUtils.readToString(req.getReader());				
		
		if (body.split("=")[0].equals("text")) {
			String paraula = body.split("=")[1].toUpperCase();
			process(res, "200 ok", null, paraula, null);
			
		}else {
			process(res, "400 Bad Request", null, null, "Bad Request");
		}
	}
	
	private void peticioPostJSON(HTTPRequest req, HTTPResponse res) {
		String body = HTTPUtils.readToString(req.getReader());					
		
		if (body.split("=")[0].equals("text")) {
			String paraula = body.split("=")[1].toUpperCase();
		
			JSONObject jout = new JSONObject();
			jout.put("text", paraula);
			jout.put("success", true);
			
			process(res, "200 ok", jout.toString(), null, null);
			
		}else {
			JSONObject jout = new JSONObject();
			jout.put("success", false);
			process(res, "400 Bad Request", jout.toString(), null, null);
		}
	}

}
