package m09uf3.impl.httpupper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONObject;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import lib.LoggingConfigurator;
import lib.Problems;
import lib.Reflections;
import lib.http.HTTPProcessor;
import lib.http.SocketHTTPServer;

public class HTTPUpperTest {
	
	static final Logger LOGGER = Logger.getLogger(HTTPUpperTest.class.getName());
	
	static final String HOST = "localhost";
	static final int PORT = 5512;
	
	static final String BASEURL = "http://" + HOST + ":" + PORT;

	private static HTTPProcessor processor;
	private static SocketHTTPServer server;
	
	@BeforeAll
	static void beforeAll() {
		
		LoggingConfigurator.configure(Level.INFO);
		
		String packageName = Problems.getImplPackage(HTTPUpperTest.class);
		processor = Reflections.newInstanceOfType(
				HTTPProcessor.class, packageName + ".HTTPUpperProcessorImpl",
				new Class[] {}, 
				new Object[] {});
		
		server = new SocketHTTPServer(processor, HOST, PORT);
		server.start();		
	}
	
	@AfterAll
	static void afterAll() {
		server.stop();
	}
	
	@Test
	void testNotFound() {
		try {
			Jsoup.connect(BASEURL + "/error").ignoreContentType(true).execute();
			fail("it should fail with 404");
	
		} catch (HttpStatusException e) {
			assertEquals(404, e.getStatusCode());
		
		} catch (IOException e) {
			fail(e);
		}			
	}
	
	@Test
	void testGetOk() {
		try {
			Document doc = Jsoup.connect(BASEURL + "/get?text=prueba").get();		
			Element result = doc.selectFirst("#upper");
			assertEquals("PRUEBA", result.text());
	
		} catch (IOException e) {
			fail(e);
		}		
	}
	
	@Test
	void testGetBadRequest() {
		try {
			Jsoup.connect(BASEURL + "/get").get();
			fail("it should fail with 400");
	
		} catch (HttpStatusException e) {
			assertEquals(400, e.getStatusCode());
		
		} catch (IOException e) {
			fail(e);
		}		
	}
	
	@Test
	void testPostOk() {
		try {
			Document doc = Jsoup.connect(BASEURL + "/post").data("text", "prueba").post();		
			Element title = doc.selectFirst("#upper");
			assertEquals("PRUEBA", title.text());
	
		} catch (IOException e) {
			fail(e);
		}		
	}
	
	@Test
	void testPostBadRequest() {
		try {
			Jsoup.connect(BASEURL + "/post").post();
			fail("it should fail with 400");
	
		} catch (HttpStatusException e) {
			assertEquals(400, e.getStatusCode());
		
		} catch (IOException e) {
			fail(e);
		}		
	}
	
	@Test
	void testJsonOk() {
		try {
			Response res = Jsoup.connect(BASEURL + "/json").method(Method.POST).data("text", "prueba")
					.ignoreContentType(true).execute();
			assertEquals(200, res.statusCode());
			assertTrue(res.contentType().indexOf("application/json") != -1);
	
			JSONObject jo = new JSONObject(res.body());
			assertTrue(jo.getBoolean("success"));
			assertEquals("PRUEBA", jo.getString("text"));
			
		} catch (HttpStatusException e) {
			assertEquals(400, e.getStatusCode());
		
		} catch (IOException e) {
			fail(e);
		}		
	}
	
	@Test
	void testJsonBadRequest() {
		try {
			Response res = Jsoup.connect(BASEURL + "/json").method(Method.POST)
					.ignoreHttpErrors(true).ignoreContentType(true).execute();			
			assertEquals(400, res.statusCode());
			assertTrue(res.contentType().indexOf("application/json") != -1);
			
			JSONObject jo = new JSONObject(res.body());
			assertFalse(jo.getBoolean("success"));
					
		} catch (IOException e) {
			fail(e);
		}		
	}
}
