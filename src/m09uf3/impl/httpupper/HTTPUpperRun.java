package m09uf3.impl.httpupper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONObject;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import lib.LoggingConfigurator;
import lib.Problems;
import lib.Reflections;
import lib.http.HTTPProcessor;
import lib.http.SocketHTTPServer;
/*
 * https://jsoup.org/cookbook/
 */

public class HTTPUpperRun {
	
	static final Logger LOGGER = Logger.getLogger(HTTPUpperRun.class.getName());
	
	static final String HOST = "localhost";
	static final int PORT = 5512;
	
	static final String BASEURL = "http://" + HOST + ":" + PORT;
	
	public static void main(String[] args) throws IOException {
		
		LoggingConfigurator.configure(Level.INFO);
		
		String packageName = Problems.getImplPackage(HTTPUpperRun.class);
		HTTPProcessor processor = Reflections.newInstanceOfType(
				HTTPProcessor.class, packageName + ".HTTPUpperProcessorImpl",
				new Class[] {}, 
				new Object[] {});

		SocketHTTPServer server = new SocketHTTPServer(processor, HOST, PORT);
		server.start();
		
		try {
			// get
			
			Document doc = Jsoup.connect(BASEURL + "/get?text=prueba").get();		
			Element result = doc.selectFirst("#upper");
			LOGGER.info("get result is " + result.text());
	
			// post
			
			doc = Jsoup.connect(BASEURL + "/post").data("text", "prueba").post();		
			result = doc.selectFirst("#upper");
			LOGGER.info("post result is " + result.text());
			
			// json
			
			Response res = Jsoup.connect(BASEURL + "/json").method(Method.POST).data("text", "prueba")
					.ignoreContentType(true).execute();
			assertEquals(200, res.statusCode());
			assertTrue(res.contentType().indexOf("application/json") != -1);
	
			JSONObject jo = new JSONObject(res.body());
			LOGGER.info("json result is " + jo.toString());
			
		} catch (HttpStatusException e) {
			LOGGER.severe("status code is " + e.getStatusCode());
		
		} finally {
			server.stop();
		}
	}
}
