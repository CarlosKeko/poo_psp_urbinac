package m09uf3.impl.upper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

import m09uf3.prob.upper.ToUpperClient;

public class ToUpperClientImpl implements ToUpperClient {

	private String host;
	private int port;
	private Socket socket;
	private PrintWriter out;
	private BufferedReader in;

	static final Logger LOGGER = Logger.getLogger(ToUpperClientImpl.class.getName());
	static final Charset UTF8 = StandardCharsets.UTF_8;
	static final Charset CHARSET = UTF8;

	public ToUpperClientImpl(String host, int port) {
		this.socket = new Socket();
		this.host = host;
		this.port = port;

	}

	public String toUpper(String input) throws IOException {
		out.println(input);
		
		String respuesta = in.readLine();
		
		LOGGER.info("response is " + respuesta);
		return respuesta;

	}

	public void connect() throws IOException {
		socket.connect(new InetSocketAddress(host, port), 1500); // connect timeout
		socket.setSoTimeout(10000); // response timeout
		out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), CHARSET), true);
		in = new BufferedReader(new InputStreamReader(socket.getInputStream(), CHARSET));
	}

	public void disconnect() throws IOException {
		socket.close();
		LOGGER.info("Client socket is closed");
	}

}
