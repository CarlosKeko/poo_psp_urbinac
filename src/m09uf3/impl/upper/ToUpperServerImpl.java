package m09uf3.impl.upper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import m09uf3.prob.upper.ToUpperServer;

public class ToUpperServerImpl implements ToUpperServer, Runnable {

	private String host;
	private int port;
	private ServerSocket serverSocket;
	private ExecutorService executor;
	private Thread thread;

	static final Logger LOGGER = Logger.getLogger(ToUpperServerImpl.class.getName());
	static final int TIMEOUT_MILLIS = 3000;
	static final Charset UTF8 = StandardCharsets.UTF_8;
	static final Charset ISO8859_1 = StandardCharsets.ISO_8859_1;
	static final Charset CHARSET = UTF8;

	public ToUpperServerImpl(String host, int port) {
		this.host = host;
		this.port = port;
		this.executor = Executors.newCachedThreadPool();
		this.thread = new Thread(this, "fillPrincipal");
	}

	public void start() {
		try {
			serverSocket = new ServerSocket(port, 10, InetAddress.getByName(host));
			serverSocket.setSoTimeout(TIMEOUT_MILLIS);

			thread.start();

		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, null, e);
		}

	}

	private static void handle(Socket clientSocket) {
		LOGGER.info("Hilo ejecutado");

		SocketAddress remote = clientSocket.getRemoteSocketAddress();
		LOGGER.info("accept from " + remote);
			
		String input = null;
			try (PrintWriter out = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream(), CHARSET), true);
					BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), CHARSET))) {	
				
				while ((input = in.readLine()) != null) { 
					LOGGER.info("request is " + input);
					out.println(input.toUpperCase());
				}

				
			} catch (SocketTimeoutException e) {
				// read timeout: do nothing
				LOGGER.fine("timeout reading");
				
			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, "handling", e);
			}
		

	}

	public void stop() {
		try {
			serverSocket.close();
			LOGGER.info("Server socket is closed");

		} catch (IOException e) {
			LOGGER.info("Error al cerrar el socket del servidor");
		}
	}

	@Override
	public void run() {
		while (!serverSocket.isClosed()) {
			try {
				Socket clientSocket = serverSocket.accept();
				LOGGER.info("Cliente en linea");
				clientSocket.setSoTimeout(TIMEOUT_MILLIS);
				
				executor.execute(() -> handle(clientSocket));
				
			} catch (SocketTimeoutException e) {
				LOGGER.fine("timeout!");
				
			}catch (IOException e) {
//				e.printStackTrace();
				LOGGER.info("La sesion a finalizado, caused: " + e.getMessage());
//				LOGGER.log(Level.SEVERE, "en el run", e);
			}


		}

	}

}
