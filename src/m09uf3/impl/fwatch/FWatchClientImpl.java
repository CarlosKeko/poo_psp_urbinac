package m09uf3.impl.fwatch;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

import m09uf3.prob.fwatch.FWatchClient;
import m09uf3.prob.fwatch.FWatchEvent;
import m09uf3.prob.fwatch.FWatchHandler;

public class FWatchClientImpl implements FWatchClient, Runnable{

	private String host;
	private int port;
	private Socket socket;
	private OutputStream os = null;
	private InputStream in = null;
	private FWatchHandler handler;
	private Thread thread;
	
	static final Logger LOGGER = Logger.getLogger(FWatchClientImpl.class.getName());
	
	public FWatchClientImpl(String host, int port) {
		this.host = host;
		this.port = port;
		this.socket = new Socket();
		this.thread = new Thread(this, "fillClient");
	}
	
	public void connect(String path, FWatchHandler handler) throws IOException {
		socket.connect(new InetSocketAddress(host, port), 1500); // connect timeout
		socket.setSoTimeout(10000); // response timeout	
		os = socket.getOutputStream();
		in = socket.getInputStream();
		this.handler = handler;
		
		byte[] fileNameArray = path.getBytes(StandardCharsets.UTF_8);
		byte[] arrayEnviament = new byte[fileNameArray.length + 1];
		arrayEnviament[0] = (byte) fileNameArray.length;
		
		for (int i = 0; i < fileNameArray.length; i++) {
			arrayEnviament[i + 1] = fileNameArray[i];
		}
		
		os.write(arrayEnviament);
		thread.start();
	}

	public void disconnect() throws IOException {
		socket.close();
		thread.interrupt();
		LOGGER.info("Client socket is closed");
		
	}

	public void run() {
		
		while (true) {
			try {			
				byte[] inicialByte = new byte[] {(byte) in.read()};
				
				int count = in.read();
				byte[] nomarxiu = new byte[count];		
				
				for (int i=0; i<count; i++) {
					nomarxiu[i] = (byte) in.read();
				}
				String nomArxiu = new String(nomarxiu, StandardCharsets.UTF_8);
				String inicial = new String(inicialByte, StandardCharsets.UTF_8);

				switch (inicial) {
					case "C":
						this.handler.notify(FWatchEvent.CREATE, nomArxiu);
						break;
						
					case "D":
						this.handler.notify(FWatchEvent.DELETE, nomArxiu);
						break;
						
					case "M":
						this.handler.notify(FWatchEvent.MODIFY, nomArxiu);
						break;
					
					default:
						LOGGER.info("Error al recibir la inicial!");
						break;
				}

			} catch (IOException e) {
				
				if (socket.isClosed()) {
					break;
				}
				
				LOGGER.log(Level.SEVERE, null, e);
			}
		}
		
	}
}
