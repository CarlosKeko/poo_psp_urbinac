package m09uf3.impl.fwatch;

public enum FWatchEvent {
	CREATE,
	DELETE,
	MODIFY
}
