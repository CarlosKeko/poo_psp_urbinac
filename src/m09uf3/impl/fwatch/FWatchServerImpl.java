package m09uf3.impl.fwatch;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import lib.Startable;

public class FWatchServerImpl implements Startable, Runnable{
	
	private String host;
	private int port;
	private volatile ServerSocket serverSocket;
	private Thread thread;
	
	static final Logger LOGGER = Logger.getLogger(FWatchServerImpl.class.getName());
	
	public FWatchServerImpl(String host, int port) {
		this.host = host;
		this.port = port;
		this.thread = new Thread(this, "FillServidor");
		
	}

	public void start() {
		thread.start();
		
	}
	
	public void stop() {
		try {
			thread.interrupt();
			serverSocket.close();
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
	}

	public boolean isStarted() {
		return thread.isAlive();
	}
	
	static void handle(Socket clientSocket) {
		
		try {
			InputStream in = clientSocket.getInputStream();
			OutputStream out = clientSocket.getOutputStream();	
			
			FWatchHandlerImpl fWatchHandler = new FWatchHandlerImpl(out);
			
			int count = in.read();
			byte[] nomarxiu = new byte[count];
			for (int i=0; i<count; i++) {
				nomarxiu[i] = (byte) in.read();
			}
			
			String nomarxiuStr = new String(nomarxiu, StandardCharsets.UTF_8);
			
			FolderWatcher folderWatcher = new FolderWatcher(nomarxiuStr, fWatchHandler);
			
			folderWatcher.start();
			
			while (true) {
				try {
					if (in.read() == -1) {
						LOGGER.info("end of stream!");
						break;
					}
				}catch (SocketTimeoutException e) {
					LOGGER.fine("Timeout waiting");
				}
			}
			
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "serving", e);
		}

		
	}
	
	public void run() {
		ExecutorService executor = Executors.newCachedThreadPool();
		
		try {
			serverSocket = new ServerSocket(port, 10, InetAddress.getByName(host));
			serverSocket.setSoTimeout(3000);
			
			while (true) {
				try {
					Socket clientSocket = serverSocket.accept();
					
					SocketAddress remote = clientSocket.getRemoteSocketAddress();					
					LOGGER.info("accept from " + remote);
					
					clientSocket.setSoTimeout(3000);	
					
					executor.execute(() -> handle(clientSocket));
					
				}catch (SocketTimeoutException e ) {
//					LOGGER.log(Level.SEVERE, "serving", e);
					LOGGER.fine("timeout accepting!");
				}
			}
			
		} catch (IOException e) {
			if (serverSocket.isClosed()) {
				LOGGER.info("server was closed");
				return;
			}
			
			LOGGER.log(Level.SEVERE, "serving", e);
		
		} finally {
			executor.shutdown();
			
		}
		
	}

}
