package m09uf3.impl.fwatch;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

import m09uf3.prob.fwatch.FWatchEvent;
import m09uf3.prob.fwatch.FWatchHandler;

public class FWatchHandlerImpl implements FWatchHandler{

	private OutputStream out;
	
	static final Logger LOGGER = Logger.getLogger(FWatchHandlerImpl.class.getName());
	
	public FWatchHandlerImpl(OutputStream out) {
		this.out = out;
	}
	
	public void notify(FWatchEvent event, String filePath) {
		try {
			
			LOGGER.info("sending " + event + " on " + filePath);
			String inicial = null;
			switch (event) {
				case CREATE:
					inicial = "C";
					break;
					
				case DELETE:
					inicial = "D";
					break;
					
				case MODIFY:
					inicial = "M";
					break;
			}
			
			byte[] fileNameArray = filePath.getBytes(StandardCharsets.UTF_8);
			byte[] arrayEnviament = new byte[fileNameArray.length + 2];
			byte[] aux = inicial.getBytes(StandardCharsets.UTF_8);
			arrayEnviament[0] = aux[0];
			arrayEnviament[1] = (byte) fileNameArray.length;
			
			for (int i = 0; i<fileNameArray.length; i++) {
				arrayEnviament[i + 2] = fileNameArray[i];
			}
			
			out.write(arrayEnviament);
			
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "serving", e);
		}
		
	}

}
